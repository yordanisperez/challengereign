import { Module } from '@nestjs/common';
import { StudentModule } from '../student/student.module';
import { TeacherModule } from '../theacher/teacher.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { ApiReignModule } from '../notice/notice.module';

@Module({
  imports: [
    StudentModule,
    TeacherModule,
    ApiReignModule,

    //ApiReignConsumerModule,
    ConfigModule.forRoot({ envFilePath: `.${process.env.NODE_ENV}.env` }),
    MongooseModule.forRoot(
      `mongodb://${process.env.HOST}/${process.env.DATABASE}`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
    ),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
