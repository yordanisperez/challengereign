import { NoticeDto } from '../dto/notice.dto';
import { HttpService } from '@nestjs/common';

import { Injectable } from '@nestjs/common';
import { iConsumerApi } from '../interface/apiConsumer.interface';
/**
 * A implementación update request from axios in @nestjs/common, library deprecate
 */
@Injectable()
export class consumerApiReignDeprecate implements iConsumerApi {
  constructor(private httpService: HttpService) {}

  setHttpService(httpService: any) {
    this.httpService = httpService;
  }
  async getJsonApi(url: string): Promise<NoticeDto[]> {
    const responseAxios = await this.httpService.get(url).toPromise();

    const resp: NoticeDto[] = responseAxios.data.hits.map((itNot) => {
      const {
        created_at,
        title,
        url,
        author,
        story_title,
        story_url,
        ...rest
      } = itNot;
      return { created_at, title, url, author, story_title, story_url };
    });
    return resp;
  }
}
