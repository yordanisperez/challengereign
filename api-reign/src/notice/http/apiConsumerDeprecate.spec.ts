import { HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { consumerApiReignDeprecate } from './apiConsumerDeprecate';

describe('ApiConsumerDeprecate', () => {
  let consumerDeprecate: consumerApiReignDeprecate;
  const dataTesting = {
    created_at: '2021-07-07T16:00:20.000Z',
    title: 'Node.js – v16.9.0',
    url: null,
    author: 'istingray',
    story_title:
      "ProtonMail deletes 'we don't log your IP' from website after activist arrested",
    story_url:
      'https://www.theregister.com/2021/09/07/protonmail_hands_user_ip_address_police/',
  };
  const MockHttpService = {
    get: jest.fn().mockImplementation(() => {
      function toPromise() {
        return Promise.resolve({ data: { hits: [dataTesting] } });
      }
      return {
        toPromise: jest.fn().mockImplementation(() => {
          return toPromise();
        }),
      };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        consumerApiReignDeprecate,
        {
          provide: HttpService,
          useValue: MockHttpService,
        },
      ],
    }).compile();

    // instantiate Simulated consumerDeprecate service
    consumerDeprecate = module.get<consumerApiReignDeprecate>(
      consumerApiReignDeprecate,
    );
  });
  it('01-should is defined', () => {
    expect(consumerDeprecate).toBeDefined();
  });
  it('02-The function getJsonApi should is defined', () => {
    expect(consumerDeprecate.getJsonApi).toBeDefined();
  });
  it('03-The function getJsonApi should retrieve a array of dataTesting', async () => {
    expect(await consumerDeprecate.getJsonApi('The url')).toEqual([
      dataTesting,
    ]);
    expect(MockHttpService.get).toHaveBeenCalled();
    expect(MockHttpService.get).toHaveBeenCalledWith('The url');
  });
});
