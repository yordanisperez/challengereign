import "@testing-library/jest-dom/extend-expect";
import { render, RenderResult } from "@testing-library/react";
import Navigation from "./Navigation";
import NoticeContext from "../store/NoticeContext";
import { fireEvent } from "@testing-library/dom";
import { AppContextInterfaceNotice } from "../store/NoticeContext";
import { BrowserRouter } from "react-router-dom";

describe("Testing Component Navigation", () => {
  const mockSetActionLoadData: () => boolean = jest.fn();
  let component: RenderResult;
  beforeEach(() => {
    component = render(
      <NoticeContext.Provider
        value={
          new AppContextInterfaceNotice(
            jest.fn(),
            jest.fn(),
            jest.fn(),
            mockSetActionLoadData,
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn()
          )
        }
      >
        <div>
          <BrowserRouter>
            <Navigation></Navigation>
          </BrowserRouter>
        </div>
      </NoticeContext.Provider>
    );
  });
  test("Navigation is rendering", () => {
    component.getByText("Challenge Reign Full Stack Developed");
    component.getByText("Load now");
  });
  test("When cliking Load Now is call function setActionLoadData", () => {
    const loadNow = component.getByText("Load now");
    fireEvent.click(loadNow);
    expect(mockSetActionLoadData).toBeCalled();
  });
});
