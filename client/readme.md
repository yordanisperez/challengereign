# Install Cliente 
Run comand inside folder /cliente

>   `npm install`




# Mode Developed
The aplication not was create using  `npx create-react-app`, this is for build custom configuring, its are hidden with the method `create-react-app`, 

Run comand 


>  `npm run start`

Exist one file call `developed.env`, it is need for start the app in mode developed.It have the variable of eviroment `REACT_APP_API=http://localhost:8080`. 
All the configure was using webpack `/config/webpack.common.js /config/webpack.develop.js`



# Mode production
>   `npm run build` 

This comand create a folder call `/build` with all the files need for run the aplication in `mode production`.

You should know that exist one variable of enviroment call `REACT_APP_API`. The server provider hosting should can handle the remplace `REACT_APP_API` for el domain where is el `REST API`  `REACT_APP_API/api/notice` and `REACT_APP_API/api/removenotice`. 
Exist one file call `production.env`, it is need for build the app.

`REACT_APP_API/api/notice` get all notice available.

`REACT_APP_API/api/removenotice` delete the notice select.

All the configure was using webpack `/config/webpack.common.js /config/webpack.production.js`

## Alert

In production when is build the app with docker-compose is need the file `production.env` with the name of `REACT_APP_API`, this is because docker-compose compile the app full, for get the folder `/build`. Only the `/build` is copy to image finish of `alpine` and hosting for the api in the `home` or `/`. 