import GitHubIcon from "../github.svg";
import FacebookIcon from "../facebook.svg";
import EmailIcon from "../email.svg";
import CallIcon from "../call.svg";
import "./footer.scss";

function Footer(): JSX.Element {
  return (
    <div id="Contacto">
      <ul>
        <li>
          <a
            target="_blank"
            id="profile-link"
            className="nav-link"
            href="https://m.facebook.com/yordanis.perez"
            rel="noreferrer"
          >
            <i>
              <img src={FacebookIcon} width="30" alt="Reign Logo"></img>Facebook
            </i>
          </a>
        </li>
        <li>
          <a
            target="_blank"
            id="profile-link"
            className="nav-link"
            href="https://github.com/yordanisperez"
            rel="noreferrer"
          >
            <i>
              <img src={GitHubIcon} width="30" alt="Reign Logo"></img>GitHub
            </i>
          </a>
        </li>
        <li>
          <a target="_blank" className="nav-link" href="5351497178">
            <i>
              <img src={CallIcon} width="30" alt="Reign Logo"></img>Llamame
            </i>
          </a>
        </li>
        <li>
          <a target="_blank" className="nav-link" href="yordanis@gmail.com">
            <i>
              <img src={EmailIcon} width="30" alt="Reign Logo"></img>Mail
            </i>
          </a>
        </li>
      </ul>
      <span>Copyright 2021, Original Challenge Reign</span>
    </div>
  );
}

export default Footer;
