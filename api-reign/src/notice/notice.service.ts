import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Model, Connection } from 'mongoose';
import { DeleteNoticeDto, NoticeDto } from './dto/notice.dto';
import { ApiConsumer } from './http/apiConsumer.decorator';
import { iConsumerApi } from './interface/apiConsumer.interface';
import { iNotice } from './interface/notice.interface';

const TIMEOUT = 3600000; /*3600000===1h*/

@Injectable()
export class ApiReignNotice {
  descripUpdate: any;
  /**
   *
   * @param consumer
   * @param noticeModel
   * @param connection
   */
  constructor(
    @ApiConsumer('API_CONSUMER_AXIOS')
    private readonly consumer: iConsumerApi,
    @InjectModel('Notice') private noticeModel: Model<iNotice>,
    @InjectConnection() private readonly connection: Connection,
  ) {
    this.descripUpdate = setTimeout(async () => {
      await this.updateApi();
    }, 0);
  }
  /**
   *
   * @returns
   */
  async getNotices(): Promise<NoticeDto[]> {
    return await this.noticeModel.find();
  }
  async deleteNotice(idNotice: string): Promise<DeleteNoticeDto> {
    return await this.noticeModel.deleteOne({ _id: idNotice });
  }
  /**
   *
   */
  async inicialiceApi(): Promise<void> {
    const notices: NoticeDto[] = await this.consumer.getJsonApi(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
    notices.map(async (not) => {
      const docNot = await this.noticeModel.create(not);
      await docNot.save();
    });
  }
  /**
   *
   */
  async clearApi() {
    await this.noticeModel.deleteMany();
  }
  /**
   *
   */
  async updateApi(): Promise<void> {
    clearTimeout(this.descripUpdate);
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      await this.clearApi();
      await this.inicialiceApi();
    } catch (error) {
      await session.abortTransaction();
      this.descripUpdate = setTimeout(async () => {
        await this.updateApi();
      }, 10000);
      console.log('Verify your conection');
    } finally {
      session.endSession();
      this.descripUpdate = setTimeout(async () => {
        await this.updateApi();
      }, TIMEOUT);
    }
    //
  }
}
