import {
  Body,
  Controller,
  Get,
  Param,
  //ParseUUIDPipe,
  Post,
  Put,
} from '@nestjs/common';
import {
  ApiCreatedResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  CreateStudentDto,
  FindStudentResponceDto,
  StudentResponceDto,
  UpdateStudentDto,
} from './dto/student.dto';
import { StudentService } from './student.service';

@ApiTags('Students')
@Controller('students')
export class StudentController {
  constructor(private readonly studentService: StudentService) {}

  /**
   * @return FindStudentResponceDto[] -Retrieve all student
   *  */
  @ApiOperation({ summary: 'Retrieve all students' })
  @ApiResponse({
    status: 200,
    description: 'The operation make run success ',
  })
  @Get()
  getStudents(): Promise<FindStudentResponceDto[]> {
    return this.studentService.getStudents();
  }
  /**
   * @param {string} studentId -Id of student
   * @return {FindStudentResponceDto} -the student if id equal studentId
   */
  @ApiCreatedResponse({ type: FindStudentResponceDto })
  @Get('/:studentID')
  async getStudentByID(
    @Param('studentID' /*, new ParseUUIDPipe()*/) studentId: string,
  ): Promise<FindStudentResponceDto> {
    return await this.studentService.getStudentByID(studentId);
  }
  /**
   *
   * @param body
   * @returns
   */
  @ApiCreatedResponse({ type: StudentResponceDto })
  @Post()
  async createStudent(
    @Body() body: CreateStudentDto,
  ): Promise<StudentResponceDto> {
    return await this.studentService.createStudent(body);
  }
  @ApiCreatedResponse({ type: StudentResponceDto })
  @Put('/:studentID')
  async updateStudent(
    @Param('studentID' /*, new ParseUUIDPipe()*/) studentId: string,
    @Body() body: UpdateStudentDto,
  ): Promise<StudentResponceDto> {
    return await this.studentService.updateStudent(studentId, body);
  }
}
