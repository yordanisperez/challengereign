const notice = require("../crud/notice");
const TIMEOUT = 10000;

function handleGetNotice(req, res, next) {
  console.log("Handle Request handleGetNotice  call ");
  try {
    let t = setTimeout(() => {
      next({ message: "timeout" });
    }, TIMEOUT);
    notice.getAllNoticeSort((error, data) => {
      clearTimeout(t);

      if (error) {
        console.log("error: ", error);
        return next(error);
      }
      if (data) {
        res.status(200).json(data);
      } else {
        res.status(200).json({ Message: "No news found in the database " });
      }
    });
  } catch (error) {
    console.log(error);
    res.status(401).json({ Error: "A error, try more later" });
  }
}

exports.handleGetNotice = handleGetNotice;
