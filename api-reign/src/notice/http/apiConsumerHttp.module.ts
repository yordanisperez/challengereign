import { DynamicModule, HttpModule } from '@nestjs/common';
import { HttpModule as HttpModuleAxios } from '@nestjs/axios';

import {
  createConsumersProviders,
  createProviders,
} from './apiConsumer.providers';
import './apiConsumerDeprecate';

export class ApiConsumerModule {
  static forRoot(): DynamicModule {
    const httpConsumerProvider = createConsumersProviders();
    const providers = createProviders();
    return {
      module: ApiConsumerModule,
      imports: [HttpModule, HttpModuleAxios],
      providers: [...providers, ...httpConsumerProvider],
      exports: [...providers, ...httpConsumerProvider],
    };
  }
}
