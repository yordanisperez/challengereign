import "@testing-library/jest-dom/extend-expect";
import { render, RenderResult } from "@testing-library/react";

import NoticeContext from "./store/NoticeContext";

import { AppContextInterfaceNotice } from "./store/NoticeContext";
//import { prettyDOM } from "@testing-library/dom";
import { App } from "./App";

describe("App", () => {
  const getNotices = jest.fn().mockImplementation(() => {
    return [
      {
        _id: "10",
        created_at: new Date("2021-07-07T16:00:20.000Z"),
        title: "This is Title",
        url: null,
        author: "This is a author",
        story_title: "This a story title",
        story_url: "This a story url",
      },
    ];
  });
  const isLoading = jest.fn();

  let component: RenderResult;
  beforeEach(() => {});

  test(" is rendering all app", () => {
    isLoading.mockImplementation(() => {
      return false;
    });
    component = render(
      <NoticeContext.Provider
        value={
          new AppContextInterfaceNotice(
            getNotices,
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            isLoading,
            jest.fn(),
            jest.fn()
          )
        }
      >
        <App></App>
      </NoticeContext.Provider>
    );
    // console.log(prettyDOM(component.container));
    component.getByText("This is Title");
    component.getByText("-This is a author-");
    component.getByText("Wednesday, Jul 07");
    component.getByText("Challenge Reign Full Stack Developed");
    component.getByText("Load now");
    component.getByText("Facebook");
    component.getByText("GitHub");
    component.getByText("Copyright 2021, Original Challenge Reign");
  });

  test("is rendering Backdrop", () => {
    isLoading.mockImplementation(() => {
      return true;
    });
    component = render(
      <NoticeContext.Provider
        value={
          new AppContextInterfaceNotice(
            getNotices,
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            isLoading,
            jest.fn(),
            jest.fn()
          )
        }
      >
        <App></App>
      </NoticeContext.Provider>
    );
    // console.log(prettyDOM(component.container));
    component.getByText("Loading ...");
  });
});
