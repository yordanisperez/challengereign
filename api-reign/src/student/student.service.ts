import { Injectable } from '@nestjs/common';
import { students } from '../db';
//import { v4 as uuid } from 'uuid';
import {
  CreateStudentDto,
  FindStudentResponceDto,
  StudentResponceDto,
  UpdateStudentDto,
} from './dto/student.dto';
import { iStudent } from './student.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class StudentService {
  constructor(
    @InjectModel('Student')
    private studentModel: Model<iStudent>,
  ) {}
  private students = students;
  /**
   * @description -getStudents get all students
   * @returns {FindStudentResponceDto[]} A array with all students
   */
  async getStudents(): Promise<FindStudentResponceDto[] | undefined> {
    const studentDoc: iStudent[] = await this.studentModel.find();
    if (studentDoc)
      return studentDoc.map((itStud) => {
        const objStudent: FindStudentResponceDto = {
          name: itStud.name,
          _id: itStud._id,
          teacher: itStud.teacher,
        };

        return objStudent;
      });
    return undefined;
  }
  /**
   * @description -Get a student for id
   * @param {string} studentId -Id that identify the student a get
   * @return {FindStudentResponceDto} -A student to return
   */
  async getStudentByID(
    studentId: string,
  ): Promise<FindStudentResponceDto | undefined> {
    const docStudent: iStudent = await this.studentModel.findOne({
      _id: studentId,
    });
    if (docStudent)
      return {
        name: docStudent.name,
        _id: docStudent._id,
        teacher: docStudent.teacher,
      };
    return undefined;
  }
  /**
   * @description -Create a new students
   * @param {CreateStudentDto} payload -Data student send in body request
   * @returns {Promise<StudentResponceDto>} -Retrieve object student create
   */
  async createStudent(payload: CreateStudentDto): Promise<StudentResponceDto> {
    const newStudent: iStudent = await this.studentModel.create(payload);
    const saveStudent: iStudent = await newStudent.save();

    return {
      _id: saveStudent._id,
      teacher: saveStudent.teacher,
      name: saveStudent.name,
    };
  }
  /**
   *
   * @param {string} idStudent -id of student to update
   * @param {UpdateStudentDto} payload   -Info of student to update
   * @returns {StudentResponceDto} -Responce data student update
   */
  async updateStudent(
    idStudent: string,
    payload: UpdateStudentDto,
  ): Promise<StudentResponceDto> {
    const studentUpdate: iStudent = await this.studentModel.findByIdAndUpdate(
      idStudent,
      payload,
      { new: true },
    );
    return {
      _id: studentUpdate._id,
      teacher: studentUpdate.teacher,
      name: studentUpdate.name,
    };
    /*
    const studentUpdate = {
      _id: idStudent,
      ...payload,
    };
    const index = this.students.findIndex(
      (itStudent) => itStudent._id === idStudent,
    );
    students[index] = studentUpdate;
    return studentUpdate;
    */
  }
  /**
   *
   * @param idTeacher
   * @returns
   */
  async getStudentForTeacherId(
    idTeacher: string,
  ): Promise<FindStudentResponceDto[] | undefined> {
    const listDocStudentFind: iStudent[] = await this.studentModel.find({
      teacher: idTeacher,
    });

    if (listDocStudentFind)
      return listDocStudentFind.map((itDoc) => {
        return {
          _id: itDoc._id,
          name: itDoc.name,
          teacher: itDoc.teacher,
        };
      });
    return undefined;
  }
  /**
   *
   * @param teacherId
   * @param studentId
   * @returns
   */
  async updateStudentTeacherId(
    teacherId: string,
    studentId: string,
  ): Promise<StudentResponceDto> {
    const docStudentUpdate: iStudent =
      await this.studentModel.findByIdAndUpdate(
        studentId,
        {
          teacher: teacherId,
        },
        { new: true },
      );
    return {
      _id: docStudentUpdate._id,
      name: docStudentUpdate.name,
      teacher: docStudentUpdate.teacher,
    };
    /*
    const index = this.students.findIndex(
      (itStudent) => itStudent._id === studentId,
    );
    const studentUpdate = {
      ...students[index],
      teacher: teacherId,
    };
    students[index] = studentUpdate;
    return studentUpdate;
    */
  }
}
