
# Install server 
Run comand inside folder /server

>   `npm install`

# Mode Developed
>  `npm run dev`


The server should set `PORT` `URL_ALLOW` `DB_URI` enviroment variable for mode developed

## sample
``` js
PORT=8080
MONGO_HOST='http://localhost:3000'
DB_URI='mongodb://localhost:27017/ChallengeReign?readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false'
```

`URL_ALLOW`  is need for set in cors.
`MONGO_HOST` is need for conect to database
`PORT`       is the port in listener the api rest

# Mode Production
>  `npm run start`

The server not need the file `.env` if you build the app with `docker-compose`. `docker-compose` build for you a container for mongo and set enviroment variable in `docker-compose.yml`

`PORT` and `URL_ALLOW` not using in this mode, PORT if using PORT distint to `8080` you should set in docker-compose and expose in `Dockerfile`

`URL_ALLOW` Not is need id allow that api hosting `/client/build` for default, in otherwise if is need set in `docker-compose.yml`

