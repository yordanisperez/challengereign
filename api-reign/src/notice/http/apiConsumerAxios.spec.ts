import { HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { consumerApiAxios } from './apiConsumerAxios';
import { of } from 'rxjs';

describe('consumerApiAxios', () => {
  let consumerAxios: consumerApiAxios;
  const dataTesting = {
    created_at: '2021-07-07T16:00:20.000Z',
    title: 'Node.js – v16.9.0',
    url: null,
    author: 'istingray',
    story_title:
      "ProtonMail deletes 'we don't log your IP' from website after activist arrested",
    story_url:
      'https://www.theregister.com/2021/09/07/protonmail_hands_user_ip_address_police/',
  };
  const MockHttpService = {
    get: jest.fn().mockImplementation(() => {
      return of({ data: { hits: [dataTesting] } });
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        consumerApiAxios,
        {
          provide: HttpService,
          useValue: MockHttpService,
        },
      ],
    }).compile();

    // instantiate Simulated consumerDeprecate service
    consumerAxios = module.get<consumerApiAxios>(consumerApiAxios);
  });
  it('01-should is defined', () => {
    expect(consumerAxios).toBeDefined();
  });
  it('02-The function getJsonApi should is defined', () => {
    expect(consumerAxios.getJsonApi).toBeDefined();
  });
  it('03-The function getJsonApi should retrieve a array of dataTesting', async () => {
    expect(await consumerAxios.getJsonApi('The url')).toEqual([dataTesting]);
    expect(MockHttpService.get).toHaveBeenCalled();
    expect(MockHttpService.get).toHaveBeenCalledWith('The url');
  });
});
