import "./generales.scss";
import RowNotice from "./RowNotice";
import NoticeContext, {
  AppContextInterfaceNotice,
} from "../store/NoticeContext";
import { useContext } from "react";

function Generales() {
  const { getNotices } = useContext<AppContextInterfaceNotice>(NoticeContext);

  return (
    <div className="generales">
      {getNotices().map((itNotice) => {
        return <RowNotice key={itNotice._id} {...itNotice}></RowNotice>;
      })}
    </div>
  );
}

export default Generales;
