const express = require("express");
const { handleGetNotice } = require("../handleRoutes/handleGetNotice");
const { handleRemoveNotice } = require("../handleRoutes/handleRemoveNotice");
const path = require("path");

const router = express.Router((option = {}));

module.exports = function (req, res, next) {
  // Router factory

  router.get("/", (req, res, next) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/../../client/build/index.html"));
  });
  router.get("/api/notice", handleGetNotice);
  router.get("/api/removenotice", handleRemoveNotice);

  return router;
};
