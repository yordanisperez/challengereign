import { tokensApisConsumers, ApiConsumer } from './apiConsumer.decorator';

describe('ApiConsumer (Decorator)', () => {
  beforeEach(() => {
    tokensApisConsumers.splice(0);
  });
  it('01-should add the consumer to an array', () => {
    expect(ApiConsumer('API_CONSUMER_DEPRECATE')).toBeInstanceOf(Function);
    expect(tokensApisConsumers).toEqual(['API_CONSUMER_DEPRECATE']);
  });

  it('02-should not add duplicate consumer to an array', () => {
    ApiConsumer('API_CONSUMER_DEPRECATE');
    ApiConsumer('API_CONSUMER_DEPRECATE');
    expect(tokensApisConsumers).toEqual(['API_CONSUMER_DEPRECATE']);
  });
  it('03-should not add consumer that not provide implementationto an array and retrieve undefined', () => {
    try {
      expect(ApiConsumer('API_CONSUMER_NOT_IMPLEMENTATION')).toBeUndefined();
    } catch (e) {
      expect(e).toBeInstanceOf(Error);
    }
    expect(tokensApisConsumers).toEqual([]);
  });
});
