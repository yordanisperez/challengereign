import "@testing-library/jest-dom/extend-expect";
import { render, RenderResult } from "@testing-library/react";
import Generales from "./Generales";
import NoticeContext from "../store/NoticeContext";
import { iNotice } from "../interface/notice.interface";
import { AppContextInterfaceNotice } from "../store/NoticeContext";

jest.mock("src/component/RowNotice.tsx", () => {
  return {
    __esModule: true,
    default: () => {
      return <div>Row Notice Mocking</div>;
    },
  };
});

describe("Testing Component Generales", () => {
  function getNotices(): iNotice[] {
    return [
      {
        _id: "10",
        created_at: new Date("2021-07-07T16:00:20.000Z"),
        title: "This is Title",
        url: null,
        author: "This is a author",
        story_title: "This a story title",
        story_url: "This a story url",
      },
    ];
  }
  let component: RenderResult;
  beforeAll(() => {
    component = render(
      <NoticeContext.Provider
        value={
          new AppContextInterfaceNotice(
            getNotices,
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn()
          )
        }
      >
        <Generales />
      </NoticeContext.Provider>
    );
  });
  test("Generales is rendering", () => {
    component.getByText("Row Notice Mocking");
  });
});
