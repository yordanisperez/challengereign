import { useContext, useEffect } from "react";
import NoticeContext, {
  AppContextInterfaceNotice,
} from "../store/NoticeContext";

const serverApi = process.env.REACT_APP_API + "/api/notice";
/**
 * @description A hooks that listener actionLoadData
 * @returns A boolean for know if is loading data.
 */
function useIsInicialiceApi(): boolean {
  const { actionLoadData, setActionLoadData, setIsLoading, setListNotices } =
    useContext<AppContextInterfaceNotice>(NoticeContext);
  const actionLoadDat = actionLoadData();
  useEffect(() => {
    if (actionLoadDat) {
      setActionLoadData(false);
      setIsLoading(true);

      fetch(serverApi, {})
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          setIsLoading(false);
          setListNotices(data);
        })
        .catch((error) => {
          console.log(`Error Loading  from ${serverApi}: `, error);
          setIsLoading(false);
        });
    }
  }, [actionLoadData]);
  return actionLoadDat;
}

export default useIsInicialiceApi;
