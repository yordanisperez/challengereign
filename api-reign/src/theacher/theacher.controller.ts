import { Controller, Get, Param, ParseUUIDPipe, Put } from '@nestjs/common';
import { FindTeacherResponseDto } from './dto/teacher.dto';
import { TeacherService } from './teacher.service';

@Controller('teacher')
export class TeacherController {
  constructor(private readonly teacherService: TeacherService) {}
  /**
   *
   * @returns
   */
  @Get()
  getTeachers(): FindTeacherResponseDto[] {
    return this.teacherService.getTeachers();
  }
  /**
   *
   * @param teacherId
   * @returns
   */
  @Get('/:teacherId')
  getTeacherByID(
    @Param('teacherId', new ParseUUIDPipe()) teacherId: string,
  ): FindTeacherResponseDto {
    return this.teacherService.getTeacherByID(teacherId);
  }
}
