const dataTesting = {
  hits: [
    {
      created_at: "2021-07-07T16:00:20.000Z",
      title: "Node.js – v16.9.0",
      url: null,
      author: "istingray",
      story_title:
        "ProtonMail deletes 'we don't log your IP' from website after activist arrested",
      story_url:
        "https://www.theregister.com/2021/09/07/protonmail_hands_user_ip_address_police/",
    },
    {
      created_at: "2021-09-07T12:57:32.000Z",
      title: "Node.js – v16.9.0",
      url: "https://nodejs.org/en/blog/release/v16.9.0/",
      author: "bricss",
      story_title: null,
      story_url: null,
    },
    {
      created_at: "2021-05-07T12:57:32.000Z",
      title: "Node.js – v16.9.0",
      url: "https://nodejs.org/en/blog/release/v16.9.0/",
      author: "Pepe",
      story_title: null,
      story_url: null,
    },
    {
      created_at: "2021-08-07T16:00:20.000Z",
      url: null,
      author: "juan",
      story_title:
        "ProtonMail deletes 'we don't log your IP' from website after activist arrested",
      story_url:
        "https://www.theregister.com/2021/09/07/protonmail_hands_user_ip_address_police/",
    },
    {
      created_at: "2021-06-07T12:57:32.000Z",
      title: "Node.js – v16.9.0",
      url: "https://nodejs.org/en/blog/release/v16.9.0/",
      author: "pedro",
      story_title: null,
      story_url: null,
    },
  ],
};
exports.dataTesting = dataTesting;
