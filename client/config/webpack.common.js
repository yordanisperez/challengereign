/* eslint-disable @typescript-eslint/no-var-requires */

/** @type {import('webpack').Configuration} */
const path = require("path");

const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
  devServer: {
    port: 3000,
  },
  entry: {
    main: {
      import: "./src/index.tsx",

      dependOn: ["vendor_react"],
    },
    vendor_react: {
      import: ["react", "react-dom", "react-router-dom"],
    },
  },

  output: {
    path: path.resolve(__dirname, "../build"),
    filename: "[name][contenthash].js",
    publicPath: "",
    clean: true,
  },
  module: {
    rules: [
      /* {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },*/
      {
        test: /\.(js|jsx|ts|tsx)$/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/preset-typescript"],
          },
        },
        exclude: /node_modules/,
      },

      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: "css-loader",
          },
          {
            loader: "sass-loader",
          },
        ],
      },

      {
        type: "asset",
        test: /\.(png|svg|jpg|jpeg|gif|ico)$/i,
        // include: path.resolve(__dirname, '../src'),
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      favicon: "./src/LogoReign.svg",
    }),
    new MiniCssExtractPlugin({
      filename: "bundle.css",
    }),
    new CleanWebpackPlugin(),
  ],
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".jsx", ".json", ".svg"],
  },
  /* context: path.resolve(__dirname, "../src"),
  resolve: {
    modules: ["src", "node_modules"],
  },*/
};
