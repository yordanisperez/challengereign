import { Test, TestingModule } from '@nestjs/testing';
import { NoticeDto } from './dto/notice.dto';
import { ApiReignController } from './notice.controller';
import { ApiReignNotice } from './notice.service';

describe('StudentController', () => {
  //defined the controller
  let controller: ApiReignController;
  //Dto for testing
  const noticeDto: NoticeDto = {
    _id: '10',
    created_at: new Date('2021-07-07T16:00:20.000Z'),
    title: 'This is Title 1',
    url: null,
    author: 'This is a author 1',
    story_title: 'This a story title 1',
    story_url: 'This a story url 1',
  };
  //Local service for testing
  const noticeService = {
    getNotices: jest.fn().mockImplementation(() => {
      return Promise.resolve([noticeDto]);
    }),
    deleteNotice: jest.fn().mockImplementation(() => {
      return Promise.resolve({ deleteCount: 1 });
    }),
  };

  //Simulated student service
  let noticeMockService: ApiReignNotice;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ApiReignController],
      providers: [
        {
          provide: ApiReignNotice,
          useValue: noticeService,
        },
      ],
    }).compile();

    // instantiate  controller
    controller = module.get<ApiReignController>(ApiReignController);
    // instantiate Simulated student service
    noticeMockService = module.get<ApiReignNotice>(ApiReignNotice);
  });

  it('01-Should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('02-Should have getNotices function', () => {
    expect(controller.getNotices).toBeDefined();
  });

  it('03-The function getNotices Should Retrieve a Promise<NoticeDto[]> all notices', async () => {
    expect(await controller.getNotices()).toEqual([
      {
        ...noticeDto,
      },
    ]);
    expect(noticeMockService.getNotices).toBeCalledTimes(1);
  });

  it('04-Should have deleteNotice Api', () => {
    expect(controller.deleteNotice).toBeDefined();
  });

  it('03-The Api deleteNotice Should Retrieve a Promise<ResponceDeleteNoticeDto> ', async () => {
    expect(await controller.deleteNotice('10')).toEqual({
      state: 'success',
      message: 'The notice with noticeId = 10 was deleted success',
    });
    expect(noticeMockService.deleteNotice).toBeCalledTimes(1);
  });
});
