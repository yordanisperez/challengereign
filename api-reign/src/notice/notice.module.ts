import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ApiConsumerModule } from './http/apiConsumerHttp.module';
import { ApiReignController } from './notice.controller';
import { NoticeSchema } from './notice.schema';
import { ApiReignNotice } from './notice.service';

@Module({
  imports: [
    MongooseModule.forFeature(
      [{ name: 'Notice', schema: NoticeSchema }],
      // 'MainConection',
    ),
    ApiConsumerModule.forRoot(),
    //ApiReignConsumerModule,
  ],
  controllers: [ApiReignController],
  providers: [ApiReignNotice],
  exports: [ApiReignNotice],
})
export class ApiReignModule {}
