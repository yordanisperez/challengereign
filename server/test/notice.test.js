const supertest = require("supertest");
const { app, server, db } = require("../server");
const routes = require("../routes/routes");
const failtRoutes = require("../routes/failtRoutes");
const Notice = require("../crud/notice");
const { dataTesting } = require("./testingNotice");

const { MONGO_HOST } = process.env;
const URITEST = `mongodb://${MONGO_HOST}/ChallengeReignTest?readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false`;

const api = supertest(app);

beforeAll(async () => {
  db.connect(URITEST, (error, mongoose) => {
    if (error) {
      // se responde a cualquier ruta valida con un mensaje de servidor fuera de servicio
      console.log(error);
      app.use(failtRoutes({}));
    } else {
      const dbm = mongoose.connection;
      dbm.once("open", (_) => {
        console.log("conected: ", strHost);
      });

      dbm.on("error", (err) => {
        console.log("Ha ocurrido un error");
      });
      app.use(routes({}));

      //updateNotice();
    }
  });
});
// For each testing
beforeEach(async () => {
  for (let i = 0; i < dataTesting.hits.length; i++) {
    await Notice.createNewNotice(dataTesting.hits[i], (error, dat) => {
      if (error) throw new error("One Error insert notices");
    });
  }
});

// Cleans up database between each test
afterEach(async () => {
  await Notice.removeAllNotice((error, data) => {
    console.log(data);
  });
});

describe("Testing routing /api/notice", () => {
  test("The api notice are return in json", async () => {
    await api
      .get("/api/notice")
      .expect(200)
      .expect("Content-Type", /application\/json/);
  });

  test("The api return count 5 notice", async () => {
    const responce = await api.get("/api/notice");
    expect(responce.body).toHaveLength(5);
  });

  test("The api return the notes sorted create_at", async () => {
    const responce = await api.get("/api/notice");
    expect(responce.body[0].author).toBe("bricss");
    expect(responce.body[1].author).toBe("juan");
    expect(responce.body[2].author).toBe("istingray");
    expect(responce.body[3].author).toBe("pedro");
    expect(responce.body[4].author).toBe("Pepe");
  });

  test("If delete a notice, this not is return next", async () => {
    await Notice.removeNoticeForAuthor("juan", async (error, data) => {});
    const responce = await api.get("/api/notice");
    expect(
      responce.body.filter((it) => {
        return it.author === "juan";
      })
    ).toHaveLength(0);
  });
});

describe("Testing routing /api/removenotice", () => {
  test("The api /api/removenotice are return in json", async () => {
    await api
      .get("/api/removenotice")
      .expect(200)
      .expect("Content-Type", /application\/json/);
  });

  test("The api /api/removenotice delete id notice", async () => {
    const docs = await Notice.getAllNoticeSort((err, _docs) => {});
    console.log(docs);
    for (const notic of docs) {
      const responce = await api.get("/api/removenotice?_id=" + notic._id);
      expect(responce.body.deletedCount).toBe(1);
    }
    const docsRestantes = await Notice.getAllNoticeSort((err, _docs) => {});
    expect(docsRestantes).toHaveLength(0);
  });
});

afterAll(async () => {
  await db.close();
  await server.close();
});
