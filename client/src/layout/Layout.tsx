import Navigation from "./Navigation";
import Footer from "./Footer";
import "./layout.scss";
import { AppProps } from "../interface/notice.interface";

function Layout({ children }: AppProps): JSX.Element {
  return (
    <div className="layout">
      <Navigation></Navigation>
      <div className="layout-main">{children}</div>
      <Footer></Footer>
    </div>
  );
}
export default Layout;
