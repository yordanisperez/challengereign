import { createContext, useState, Dispatch, SetStateAction } from "react";
import { iNotice, AppProps } from "../interface/notice.interface";

export class AppContextInterfaceNotice {
  constructor(
    public getNotices: () => iNotice[],
    public setListNotices: Dispatch<SetStateAction<iNotice[]>>,
    public actionLoadData: () => boolean,
    public setActionLoadData: Dispatch<SetStateAction<boolean>>,
    public actionDeleteNotice: () => string,
    public setActDeleteNotice: Dispatch<SetStateAction<string>>,
    public isLoading: () => boolean,
    public setIsLoading: Dispatch<SetStateAction<boolean>>,
    public removeNotice: (idNotice: string) => void
  ) {}
}
const NoticeContext = createContext<AppContextInterfaceNotice | null>(null);

export function NoticeContextProvider({ children }: AppProps): JSX.Element {
  const [listNotice, setListNotice] = useState<iNotice[]>([]); // the list of notice to display
  const [actionLoadData, setActionLoadData] = useState<boolean>(true); // state for know when load data, is call async
  const [actionDeleteNotice, setActDeleteNotice] = useState<string>(""); // state for know when call api, is call async
  const [isLoading, setIsLoading] = useState<boolean>(false); // if is loading display other view

  function getNotices(): iNotice[] {
    return listNotice;
  }
  function getActionLoadData(): boolean {
    return actionLoadData;
  }
  function getActionDeleteNotice(): string {
    return actionDeleteNotice;
  }
  function getIsLoading(): boolean {
    return isLoading;
  }

  function removeNotice(idNotice: string): void {
    setListNotice(
      listNotice.filter((it) => {
        return it._id !== idNotice;
      })
    );
    setActDeleteNotice(idNotice);
  }

  const ctxNotice: AppContextInterfaceNotice = new AppContextInterfaceNotice(
    getNotices,
    setListNotice,
    getActionLoadData,
    setActionLoadData,
    getActionDeleteNotice,
    setActDeleteNotice,
    getIsLoading,
    setIsLoading,
    removeNotice
  );

  return (
    <NoticeContext.Provider value={ctxNotice}>
      {children}
    </NoticeContext.Provider>
  );
}

export default NoticeContext;
