import { HttpException, Injectable, NestMiddleware } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Request, Response, NextFunction } from 'express';
import { Model } from 'mongoose';
import { iStudent } from '../../student/student.schema';

@Injectable()
export class StudentMiddleware implements NestMiddleware {
  constructor(
    @InjectModel('Student')
    private studentModel: Model<iStudent>,
  ) {}
  /**
   * @description -Midleware valid if idStudent Exist in db.ts
   * @param req
   * @param res
   * @param next
   */
  async use(req: Request, res: Response, next: NextFunction) {
    const studentId = req.params.studentId;
    // console.log(`The id of student search ${studentId}`);
    const docStudent: iStudent = await this.studentModel.findOne({
      _id: studentId,
    });
    // console.log(`The student search ${docStudent}`);
    if (!docStudent) throw new HttpException('Student not found', 400);
    // console.log(`Ha pasado nuestro middleware`);
    next();
  }
}
