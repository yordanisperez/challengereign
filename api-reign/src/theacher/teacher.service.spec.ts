import { Test, TestingModule } from '@nestjs/testing';
import { TeacherService } from './teacher.service';

describe('TeacherService', () => {
  let teacherService: TeacherService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TeacherService],
    }).compile();
    teacherService = module.get<TeacherService>(TeacherService);
  });
  it('01-Should be defined', () => {
    expect(teacherService).toBeDefined();
  });

  it('02-Should have getTeachers function', () => {
    expect(teacherService.getTeachers).toBeDefined();
  });

  it('03-Should have getTeacherByID function', () => {
    expect(teacherService.getTeacherByID).toBeDefined();
  });

  it('04- The function getTeachers Should  retrieve a FindTeacherResponseDto[]', () => {
    expect(teacherService.getTeachers()).toEqual([
      {
        id: '9c9324e8-b656-11eb-8529-0242ac130003',
        name: 'Ms Jackson',
      },
      {
        id: '1c250754-b656-11eb-8529-0242ac130003',
        name: 'Mr Wade',
      },
    ]);
  });

  it('05- The function getTeacherByID Should  retrieve a FindTeacherResponseDto', () => {
    expect(
      teacherService.getTeacherByID('1c250754-b656-11eb-8529-0242ac130003'),
    ).toEqual({
      id: '1c250754-b656-11eb-8529-0242ac130003',
      name: 'Mr Wade',
    });
  });
});
