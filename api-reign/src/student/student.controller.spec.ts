import { Test, TestingModule } from '@nestjs/testing';
import { CreateStudentDto } from './dto/student.dto';
import { StudentController } from './student.controller';
import { StudentService } from './student.service';

describe('StudentController', () => {
  //defined the controller
  let controller: StudentController;
  //Dto for testing
  const studentDto: CreateStudentDto = {
    name: 'yordanis',
    teacher: 'Id_Teacher',
  };
  //Local service for testing
  const studentService = {
    createStudent: jest.fn().mockImplementation((studentDto) => {
      return Promise.resolve({
        id: 'Testing_ID',
        ...studentDto,
      });
    }),
    getStudents: jest.fn().mockImplementation(() => {
      return Promise.resolve([
        {
          id: 'Testing_ID',
          ...studentDto,
        },
      ]);
    }),
    getStudentByID: jest.fn().mockImplementation((id) => {
      return Promise.resolve({
        id,
        ...studentDto,
      });
    }),
    updateStudent: jest.fn().mockImplementation((id, studentDto) => {
      return Promise.resolve({
        id,
        ...studentDto,
      });
    }),
  };

  //Simulated student service
  let studentMockService: StudentService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StudentController],
      providers: [
        {
          provide: StudentService,
          useValue: studentService,
        },
      ],
    }).compile();

    // instantiate  controller
    controller = module.get<StudentController>(StudentController);
    // instantiate Simulated student service
    studentMockService = module.get<StudentService>(StudentService);
  });

  it('01-Should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('02-Should have createStudent function', () => {
    expect(controller.createStudent).toBeDefined();
  });

  it('03-Should Create a student and retrieve with ID', async () => {
    expect(await controller.createStudent(studentDto)).toEqual({
      id: 'Testing_ID',
      ...studentDto,
    });
  });

  it('04-should using StudentService.createStudent for create Student', () => {
    expect(studentMockService.createStudent).toBeCalled();
  });

  it('05-Should have getStudents function ', () => {
    expect(controller.getStudents).toBeDefined();
  });

  it('06-Should have getStudentByID function ', () => {
    expect(controller.getStudentByID).toBeDefined();
  });

  it('07-Should have updateStudent function ', () => {
    expect(controller.updateStudent).toBeDefined();
  });

  it('08-The function getStudents Should Retrieve a Promise<FindStudentResponceDto[]> all student', async () => {
    expect(await controller.getStudents()).toEqual([
      {
        id: 'Testing_ID',
        ...studentDto,
      },
    ]);
  });

  it('09- The function getStudentByID Should Retrieve a Promise<FindStudentResponceDto> ', async () => {
    expect(await controller.getStudentByID('Testing_ID')).toEqual({
      id: 'Testing_ID',
      ...studentDto,
    });
  });

  it('10- The function updateStudent Should Retrieve a Promise<StudentResponceDto> ', async () => {
    expect(await controller.updateStudent('Testing_ID', studentDto)).toEqual({
      id: 'Testing_ID',
      ...studentDto,
    });
  });
});
