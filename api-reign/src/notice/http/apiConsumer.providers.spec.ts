import {
  createConsumersProviders,
  createProviders,
} from './apiConsumer.providers';

import { tokensApisConsumers } from './apiConsumer.decorator';
import { TOKEN_API_CONSUMER } from './apiConsumer.const';

describe('apiConsumer.providers', () => {
  beforeEach(() => {
    tokensApisConsumers.splice(0);
  });
  it('01-Should have defined function createConsumersProviders ', () => {
    expect(createConsumersProviders).toBeDefined();
  });
  it('02-should create consumer providers with tokenConsumer', () => {
    tokensApisConsumers.push('API_CONSUMER_DEPRECATE');
    const providers = createConsumersProviders();

    expect(providers).toEqual([
      {
        provide: 'API_CONSUMER_DEPRECATE',
        useFactory: expect.any(Function),
        inject: expect.anything(),
      },
    ]);
  });
  it('03-Should have defined function createProviders ', () => {
    expect(createProviders).toBeDefined();
  });
  it('04-createProviders should retrieve array dependency ', () => {
    tokensApisConsumers.push('API_CONSUMER_DEPRECATE');
    const providers = createProviders();

    expect(providers).toEqual([
      TOKEN_API_CONSUMER['API_CONSUMER_DEPRECATE'].className,
    ]);
  });
});
