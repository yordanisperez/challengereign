import "@testing-library/jest-dom/extend-expect";
import { fireEvent } from "@testing-library/dom";
import { render } from "@testing-library/react";
import Backdrop from "./Backdrop";

describe("Testing Backdrop", () => {
  test("Backdrop is rendering", () => {
    const component = render(<Backdrop></Backdrop>).container.querySelector(
      "div"
    );

    expect(component).toHaveClass("backdrop");
  });
  test("Clicking in component call to event handle ", () => {
    const mockHandleOnclic = jest.fn();
    const component = render(
      <Backdrop onCancel={mockHandleOnclic}></Backdrop>
    ).container.querySelector("div");
    fireEvent.click(component);
    expect(mockHandleOnclic).toBeCalled();
  });
});
