import { Schema, Document } from 'mongoose';

export const NoticeSchema = new Schema({
  created_at: Date,
  title: String,
  url: String,
  author: String,
  story_title: String,
  story_url: String,
});
