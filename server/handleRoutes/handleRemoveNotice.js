const notice = require("../crud/notice");
const TIMEOUT = 10000;

function handleRemoveNotice(req, res, next) {
  console.log("Handle Request handleRemoveNotice  call ");
  const id_notice = req.query._id;

  try {
    let t = setTimeout(() => {
      next({ message: "timeout" });
    }, TIMEOUT);
    notice.removeNotice(id_notice, (error, data) => {
      clearTimeout(t);

      if (error) {
        console.log("error: ", error);
        return next(error);
      }
      if (data) {
        res.status(200).json(data);
      }
    });
  } catch (error) {
    console.log(error);
    res.status(401).json({ Error: "A error, try more later" });
  }
}

exports.handleRemoveNotice = handleRemoveNotice;
