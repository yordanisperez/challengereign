import { HttpService } from '@nestjs/common';
import { HttpService as HttpServiceAxios } from '@nestjs/axios';
import { consumerApiAxios } from './apiConsumerAxios';
import { consumerApiReignDeprecate } from './apiConsumerDeprecate';
/**
 * A file config for each implementation provide iConsumerApi
 * if you create a new implementation should add in this file
 * and nestjs inyect for you
 */
export const TOKEN_API_CONSUMER = {
  API_CONSUMER_DEPRECATE: {
    className: consumerApiReignDeprecate, //name of class implement abstract class iConsumerApi
    dependOn: [HttpService], //depend for inyect in dinamicModule
  },
  API_CONSUMER_AXIOS: {
    className: consumerApiAxios,
    dependOn: [HttpServiceAxios],
  },
};
