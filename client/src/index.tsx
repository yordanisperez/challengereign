import { render } from "react-dom";
import { NoticeContextProvider } from "./store/NoticeContext";
import { App } from "./App";

// const audience=process.env.REACT_APP_AUTH0_AUDIENCE

render(
  <NoticeContextProvider>
    <App />
  </NoticeContextProvider>,
  document.getElementById("app")
);
