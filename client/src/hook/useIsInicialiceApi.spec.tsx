import "@testing-library/jest-dom/extend-expect";
import { render, RenderResult } from "@testing-library/react";

import NoticeContext from "../store/NoticeContext";
import { iNotice } from "../interface/notice.interface";
import { AppContextInterfaceNotice } from "../store/NoticeContext";

import useIsInicialiceApi from "./useIsInicialiceApi";

function ListenHook(): JSX.Element {
  useIsInicialiceApi();
  return <div>This Component listen Hook</div>;
}

function setupFetchStub(data: iNotice) {
  return function fetchStub(_url: string) {
    return new Promise((resolve) => {
      resolve({
        json: () => Promise.resolve(data),
      });
    });
  };
}

describe("Hook useIsInicialiceApi", () => {
  const noticeData: iNotice = {
    _id: "10",
    created_at: new Date("2021-07-07T16:00:20.000Z"),
    title: "This is Title 1",
    url: null,
    author: "This is a author 1",
    story_title: "This a story title 1",
    story_url: "This a story url 1",
  };
  let stateActionLoadData: boolean = true;
  const actionLoadData = jest.fn().mockImplementation(() => {
    return stateActionLoadData;
  });
  const setActionLoadData = jest.fn().mockImplementation(() => {
    stateActionLoadData = false;
  });
  const setIsLoading = jest.fn();
  const setListNotices = jest.fn();
  let component: RenderResult;
  beforeEach(() => {
    //console.log(prettyDOM(component.container));
    /* jest
      .spyOn(global, "fetch")
      .mockImplementation(setupFetchStub(noticeData));*/
    global.fetch = jest.fn().mockImplementation(setupFetchStub(noticeData));
    component = render(
      <NoticeContext.Provider
        value={
          new AppContextInterfaceNotice(
            jest.fn(),
            setListNotices,
            actionLoadData,
            setActionLoadData,
            jest.fn(),
            jest.fn(),
            jest.fn(),
            setIsLoading,
            jest.fn()
          )
        }
      >
        <ListenHook></ListenHook>
      </NoticeContext.Provider>
    );
  });

  test(" is rendering", () => {
    component.getByText("This Component listen Hook");
  });

  test("Inicialize the noticeContext when run the aplication", () => {
    expect(actionLoadData).toBeCalledTimes(2);
    expect(setActionLoadData).toBeCalledTimes(1);
    expect(setIsLoading).toBeCalledTimes(2);
    expect(setListNotices).toBeCalledTimes(1);
    expect(setListNotices).toBeCalledWith(noticeData);
  });
});
