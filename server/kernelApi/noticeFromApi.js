const loadNotice = require("./loadNotice").loadNotice;
const Notice = require("../crud/notice");

const TIMEOUT = 3600000; /*3600000===1h*/
let t;
function inicialiceApi() {
  clearTimeout(t);
  try {
    loadNotice(
      "https://hn.algolia.com/api/v1/search_by_date?query=nodejs",
      (err, data) => {
        try {
          if (err) {
            throw err;
          }
          if (!data) throw new error("Not Data in object responce");
          updateNotice();
          const dataJson = JSON.parse(JSON.stringify(data));
          for (let i = 0; i < dataJson.hits.length; i++) {
            Notice.createNewNotice(dataJson.hits[i], (error, dat) => {
              if (error) throw new error("One Error insert notices");
            });
          }
        } catch (error) {
          console.log(error);
          t = setTimeout(() => {
            inicialiceApi();
          }, 100);
        }
      }
    );
  } catch (error) {
    console.log("Testing you conection to internet, Try more laster");
    t = setTimeout(() => {
      inicialiceApi();
    }, 100);
  }
}

function updateNotice() {
  t = setTimeout(() => {
    inicialiceApi();
  }, TIMEOUT);

  Notice.removeAllNotice((error, data) => {
    console.log(data);
  });
}
exports.inicialiceApi = inicialiceApi;
exports.updateNotice = updateNotice;
