import "@testing-library/jest-dom/extend-expect";
import { render, RenderResult } from "@testing-library/react";
import { fireEvent } from "@testing-library/dom";
import NoticeContext from "../store/NoticeContext";
import { iNotice } from "../interface/notice.interface";
import { AppContextInterfaceNotice } from "../store/NoticeContext";
import RowNotice from "./RowNotice";

describe("Testing Component RowNotice", () => {
  const noticeData: iNotice = {
    _id: "10",
    created_at: new Date("2021-07-07T16:00:20.000Z"),
    title: "This is Title 1",
    url: null,
    author: "This is a author 1",
    story_title: "This a story title 1",
    story_url: "This a story url 1",
  };

  const noticeData2: iNotice = {
    _id: "10",
    created_at: new Date("2021-07-07T16:00:20.000Z"),
    title: null,
    url: "This is Url 2",
    author: "This is a author 2",
    story_title: "This a story title 2",
    story_url: null,
  };
  const mockRemoveNotice = jest.fn();
  let component: RenderResult;
  beforeEach(() => {});

  test("RowNotice is rendering", () => {
    //console.log(prettyDOM(component.container));
    component = render(
      <NoticeContext.Provider
        value={
          new AppContextInterfaceNotice(
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            mockRemoveNotice
          )
        }
      >
        <RowNotice {...noticeData}></RowNotice>
        <RowNotice {...noticeData2}></RowNotice>
      </NoticeContext.Provider>
    );
    component.getByText("This is Title 1");
    component.getByText("-This is a author 1-");
    component.getByText("This a story title 2");
    component.getByText("-This is a author 2-");
  });

  test("When cliking in button remove notice is call", () => {
    component = render(
      <NoticeContext.Provider
        value={
          new AppContextInterfaceNotice(
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            mockRemoveNotice
          )
        }
      >
        <RowNotice {...noticeData}></RowNotice>
      </NoticeContext.Provider>
    );

    const componentCliking = component.getByText("This is Title 1");
    fireEvent.mouseOver(componentCliking);
    fireEvent.click(component.getByRole("button", { name: "Delete Notice" }));
    expect(mockRemoveNotice).toBeCalled();
  });
});
