import { Test, TestingModule } from '@nestjs/testing';
import { CreateStudentDto } from '../student/dto/student.dto';
import { StudentService } from '../student/student.service';
import { TeacherStudentController } from './student.controller';

describe('TeacherStudentController', () => {
  let teacherStudentController: TeacherStudentController;

  const studentDto: CreateStudentDto = {
    name: 'yordanis',
    teacher: 'teacher_id',
  };

  //Configure monk service
  const monkStudentService = {
    getStudentForTeacherId: jest.fn().mockImplementation(() => {
      return Promise.resolve([
        {
          ...studentDto,
          _id: 'student_id',
        },
      ]);
    }),
    updateStudentTeacherId: jest.fn().mockImplementation((teacher, _id) => {
      return Promise.resolve({
        ...studentDto,
        teacher,
        _id,
      });
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TeacherStudentController],
      providers: [
        {
          provide: StudentService,
          useValue: monkStudentService,
        },
      ],
    }).compile();
    // instantiate  controller
    teacherStudentController = module.get<TeacherStudentController>(
      TeacherStudentController,
    );
  });

  it('01-Should be defined', () => {
    expect(teacherStudentController).toBeDefined();
  });
  it('02-The function getTeachers Should be defined', () => {
    expect(teacherStudentController.getTeacher).toBeDefined();
  });
  it('03-The function getTeacherByID Should be defined', () => {
    expect(teacherStudentController.updateStudentTeacherId).toBeDefined();
  });

  it('04-The function getTeachers Should retrieve Promise<FindStudentResponceDto[]>', async () => {
    expect(await teacherStudentController.getTeacher('teacher_id')).toEqual([
      {
        ...studentDto,
        _id: 'student_id',
      },
    ]);
  });
  it('05-The function getTeacherByID Should retrieve Promise<StudentResponceDto>', async () => {
    expect(
      await teacherStudentController.updateStudentTeacherId(
        'teacher_id',
        'student_id',
      ),
    ).toEqual({
      ...studentDto,
      _id: 'student_id',
    });
  });
});
