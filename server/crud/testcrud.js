const test = require("../model/testModel");

const createNewTest = async function (testJson, done) {
  let doc = new test(testJson);
  await doc
    .save()
    .then((saveDoc) => {
      done(null, doc);
    })
    .catch((error) => {
      done(error, doc);
    });
  return doc;
};

exports.createNewTest = createNewTest;
