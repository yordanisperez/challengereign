# Build App (Only mode Production)
All config of `Dockerfile` and `docker-compose.yml` was build for mode production.

>   `docker-compose build`
>   `docker-compose up`

This build the app and compile all the client.

## Alert

In production when is build the app with docker-compose is need the file `production.env` with the name of `REACT_APP_API`, this is because docker-compose compile the app full, for get the folder `/build`. Only the `/build` is copy to image finish of `alpine` and hosting for the api in the `home` or `/`. For more detall view `/client/Readme.md`


The server not need the file `.env` if you build the app with `docker-compose`. `docker-compose` build for you a container for mongo and set enviroment variable in `docker-compose.yml`

`PORT` and `URL_ALLOW` not using in this mode, PORT if using PORT distint to `8080` you should set in docker-compose and expose in `Dockerfile`

`URL_ALLOW` Not is need id allow that api hosting `/client/build` for default, in otherwise if is need set in `docker-compose.yml`.

