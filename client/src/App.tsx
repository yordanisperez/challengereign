import { Fragment, useContext } from "react";

import "./index.scss";
import { Route, Switch, BrowserRouter } from "react-router-dom";

import Layout from "./layout/Layout";

import Backdrop from "./component/Backdrop";
import Generales from "./component/Generales";

import NoticeContext from "./store/NoticeContext";
import useIsInicialiceApi from "./hook/useIsInicialiceApi";
import useRemoveNoticeApi from "./hook/useRemoveNoticeApi";

export function App() {
  const { isLoading } = useContext(NoticeContext);
  useIsInicialiceApi();
  useRemoveNoticeApi();

  if (isLoading()) {
    return (
      <div className="welcome-section">
        <Backdrop></Backdrop>
        Loading ...
      </div>
    );
  }

  return (
    <BrowserRouter>
      <Fragment>
        <Layout>
          <Switch>
            <Route path="/" exact>
              <Generales />
            </Route>
          </Switch>
        </Layout>
      </Fragment>
    </BrowserRouter>
  );
}

//export default App;
