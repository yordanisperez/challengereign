import { ApiProperty } from '@nestjs/swagger';

export class NoticeDto {
  @ApiProperty()
  _id?: string;
  @ApiProperty()
  created_at: Date;
  @ApiProperty()
  title: string;
  @ApiProperty()
  url: string;
  @ApiProperty()
  author: string;
  @ApiProperty()
  story_title: string;
  @ApiProperty()
  story_url: string;
}

export class AxiosResponceDto {
  hits: NoticeDto[];
}

export class DeleteNoticeDto {
  @ApiProperty()
  deletedCount: number;
}

export class ResponceDeleteNoticeDto {
  @ApiProperty()
  state: string;
  @ApiProperty()
  message: string;
}
