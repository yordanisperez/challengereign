import { ApiProperty } from '@nestjs/swagger';

export class FindTeacherResponseDto {
  @ApiProperty()
  name: string;
  id: string;
}
export class TeacherDto {
  @ApiProperty()
  name: string;
}
