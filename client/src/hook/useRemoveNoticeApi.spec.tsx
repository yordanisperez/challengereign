import "@testing-library/jest-dom/extend-expect";
import { render, RenderResult } from "@testing-library/react";

import NoticeContext from "../store/NoticeContext";

import { AppContextInterfaceNotice } from "../store/NoticeContext";

import useRemoveNoticeApi from "./useRemoveNoticeApi";

function ListenHook(): JSX.Element {
  useRemoveNoticeApi();
  return <div>This Component listen Hook</div>;
}

function setupFetchStub(data: string) {
  return function fetchStub(_url: string) {
    return new Promise((resolve) => {
      resolve({
        json: () => Promise.resolve(data),
      });
    });
  };
}

describe("Hook useRemoveNoticeApi", () => {
  let stateActionDeleteNotice: string = "id_delete_notice";
  const actionDeleteNotice = jest.fn().mockImplementation(() => {
    return stateActionDeleteNotice;
  });
  const setActDeleteNotice = jest.fn().mockImplementation(() => {
    stateActionDeleteNotice = "";
  });

  let component: RenderResult;
  beforeEach(() => {
    //console.log(prettyDOM(component.container));
    /* jest
      .spyOn(global, "fetch")
      .mockImplementation(setupFetchStub(noticeData));*/
    global.fetch = jest
      .fn()
      .mockImplementation(setupFetchStub("id_delete_notice"));
    component = render(
      <NoticeContext.Provider
        value={
          new AppContextInterfaceNotice(
            jest.fn(),
            jest.fn(),
            jest.fn(),
            jest.fn(),
            actionDeleteNotice,
            setActDeleteNotice,
            jest.fn(),
            jest.fn(),
            jest.fn()
          )
        }
      >
        <ListenHook></ListenHook>
      </NoticeContext.Provider>
    );
  });

  test(" is rendering", () => {
    component.getByText("This Component listen Hook");
  });

  test("Delete notice of context app and send id api for delete in database", () => {
    expect(actionDeleteNotice).toBeCalledTimes(2);
    expect(setActDeleteNotice).toBeCalledTimes(1);
    expect(setActDeleteNotice).toBeCalledWith("");
  });
});
