import "./rowNotice.scss";
import DeleteIcon from "../delete.svg";
import NoticeContext, {
  AppContextInterfaceNotice,
} from "../store/NoticeContext";
import { useContext } from "react";
import { iNotice } from "../interface/notice.interface";
const options: Intl.DateTimeFormatOptions = {
  weekday: "long",
  month: "short",
  day: "2-digit",
};

function RowNotice({
  url,
  _id,
  story_url,
  story_title,
  author,
  created_at,
  title,
}: iNotice) {
  const { removeNotice } = useContext<AppContextInterfaceNotice>(NoticeContext);
  function handleOnClickRemove() {
    removeNotice(_id);
  }
  //console.log(`tHIS IS FILE SVG${JSON.stringify(DeleteIcon)}`);
  return (
    <div title="container" className="card">
      <a target="_blank" href={url ? url : story_url} rel="noreferrer">
        <div className="card-left">
          <div className="card-title">{title ? title : story_title} </div>
          <div className="card-author"> -{author}-</div>
        </div>
      </a>
      <div className="card-right">
        <div className="card-date">
          {new Date(created_at).toLocaleDateString("en-EN", options)}
        </div>
        <button className="btn" onClick={handleOnClickRemove}>
          <img
            className="card-delete"
            src={DeleteIcon}
            width="30"
            alt="Delete Notice"
          ></img>
        </button>
      </div>
    </div>
  );
}

export default RowNotice;
