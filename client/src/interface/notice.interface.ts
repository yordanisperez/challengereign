export interface iNotice {
  _id: string;
  created_at: Date;
  title: string;
  url?: string;
  author: string;
  story_title: string;
  story_url: string;
}

export declare interface AppProps {
  children: React.ReactNode;
}
