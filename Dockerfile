# --------------> The build image
#server
FROM node:lts AS build

RUN mkdir -p /usr/src/app/server

WORKDIR /usr/src/app/server

COPY ./server/package*.json /usr/src/app/server/

RUN npm ci --only=production
#cliente
RUN mkdir -p /usr/src/app/client

WORKDIR /usr/src/app/client

COPY ./client/ /usr/src/app/client/

RUN npm i 

RUN npm run build
 
# --------------> The production image
FROM node:lts-alpine@sha256:63266a0ed3c7c090e70d8bc21af85586024dd13f22a7fa30cfbac5c343836aa3

#RUN apk add dumb-init
RUN npm install pm2 -g

ENV NODE_ENV production

USER node

WORKDIR /usr/src/app

COPY --chown=node:node --from=build /usr/src/app/server/node_modules /usr/src/app/server/node_modules

COPY --chown=node:node ./server /usr/src/app/server

COPY --chown=node:node --from=build /usr/src/app/client/build /usr/src/app/client/build

EXPOSE 8080

CMD ["pm2-runtime" , "server/server.js"]