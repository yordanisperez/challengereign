import { Test, TestingModule } from '@nestjs/testing';
import { CreateStudentDto } from './dto/student.dto';
import { StudentService } from './student.service';
import { getModelToken } from '@nestjs/mongoose';
import { FilterQuery } from 'mongoose';
import { iStudent } from './student.schema';

describe('StudentService', () => {
  const studentDto: CreateStudentDto[] = [
    {
      name: 'yordanis',
      teacher: 'Id_Teacher',
    },
  ];

  const MockModelStudent = {
    find: jest.fn().mockImplementation(() => {
      if (studentDto.length === 0) return undefined;
      //const { name, teacher } = studentDto[0];
      return Promise.resolve([
        {
          _id: 'Testing_ID',
          ...studentDto[0],
        },
      ]);
    }),
    create: jest.fn().mockImplementation((payload) => {
      return Promise.resolve({
        _id: 'Testing_ID',
        ...payload,
        save: jest.fn().mockImplementation(() => {
          return Promise.resolve({
            _id: 'Testing_ID',
            ...studentDto[0],
          });
        }),
      });
    }),
    save: jest.fn().mockImplementation(() => {
      return Promise.resolve({
        _id: 'Testing_ID',
        ...studentDto[0],
      });
    }),
    findByIdAndUpdate: jest.fn().mockImplementation((_id, payload) => {
      const { teacher } = payload;
      if (payload.hasOwnProperty('name'))
        return Promise.resolve({
          _id,
          ...payload,
        });
      //console.log(`payload: ${payload} _id: ${_id} opcion: ${opcion}`);
      return Promise.resolve({
        _id,
        teacher,
        name: 'yordanis',
      });
    }),
    findOne: jest.fn().mockImplementation((obj: FilterQuery<iStudent>) => {
      const { _id } = obj;
      if (_id !== 'Testing_ID') return undefined;
      return Promise.resolve({
        _id,
        ...studentDto[0],
      });
    }),
  };

  //student service
  let studentService: StudentService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StudentService,
        {
          provide: getModelToken('Student'),
          useValue: MockModelStudent,
        },
      ],
    }).compile();

    // instantiate Simulated student service
    studentService = module.get<StudentService>(StudentService);
  });

  it('01-Should be defined', () => {
    expect(studentService).toBeDefined();
  });

  it('02-Should have getStudents function', () => {
    expect(studentService.getStudents).toBeDefined();
  });

  it('03-The getStudents function should retrieve Promise<FindStudentResponceDto[]> ', async () => {
    expect(await studentService.getStudents()).toMatchObject([
      {
        _id: 'Testing_ID',
        ...studentDto[0],
      },
    ]);
  });

  it('04-Should have createStudent function', () => {
    expect(studentService.createStudent).toBeDefined();
  });

  it('05-Should Create a student and retrieve with ID', async () => {
    expect(await studentService.createStudent(studentDto[0])).toEqual({
      _id: 'Testing_ID',
      ...studentDto[0],
    });
  });

  it('06-Should have getStudents function', () => {
    expect(studentService.getStudents).toBeDefined();
  });

  it('07-Should have getStudentByID function', () => {
    expect(studentService.getStudentByID).toBeDefined();
  });

  it('08-Should have updateStudent function', () => {
    expect(studentService.updateStudent).toBeDefined();
  });

  it('09-Should have getStudentForTeacherId function', () => {
    expect(studentService.getStudentForTeacherId).toBeDefined();
  });

  it('10-Should have updateStudentTeacherId function', () => {
    expect(studentService.updateStudentTeacherId).toBeDefined();
  });

  it('11- The function getStudents Should  retrieve a Promise<FindStudentResponceDto[]', async () => {
    expect(await studentService.getStudents()).toEqual([
      {
        _id: 'Testing_ID',
        ...studentDto[0],
      },
    ]);
  });

  it('12- The function getStudentByID Should  retrieve a Promise<FindStudentResponceDto>', async () => {
    expect(await studentService.getStudentByID('Testing_ID')).toEqual({
      _id: 'Testing_ID',
      ...studentDto[0],
    });
  });

  it('13- The function updateStudent Should  retrieve a Promise<StudentResponceDto>', async () => {
    expect(
      await studentService.updateStudent('Testing_ID', studentDto[0]),
    ).toEqual({
      _id: 'Testing_ID',
      ...studentDto[0],
    });
  });

  it('14- The function getStudentForTeacherId Should  retrieve a Promise<FindStudentResponceDto[]', async () => {
    expect(await studentService.getStudentForTeacherId('Id_Teacher')).toEqual([
      {
        _id: 'Testing_ID',
        ...studentDto[0],
      },
    ]);
  });

  it('15- The function getStudents Should  retrieve undefined if not exist students', async () => {
    studentDto.splice(0);
    expect(await studentService.getStudents()).toEqual(undefined);
  });

  it('16- The function getStudentByID Should  retrieve undefined if id not exist in student', async () => {
    expect(await studentService.getStudentByID('TestingNotId')).toEqual(
      undefined,
    );
  });

  it('17- The function getStudentForTeacherId Should  retrieve undefined if not student found', async () => {
    studentDto.splice(0);
    expect(await studentService.getStudentForTeacherId('Id_Teacher')).toEqual(
      undefined,
    );
  });

  it('18- The function updateStudentTeacherId Should  retrieve a Promise<StudentResponceDto> update', async () => {
    expect(
      await studentService.updateStudentTeacherId('Id_Teacher2', 'Testing_ID'),
    ).toEqual({
      _id: 'Testing_ID',
      teacher: 'Id_Teacher2',
      name: 'yordanis',
    });
  });
});
