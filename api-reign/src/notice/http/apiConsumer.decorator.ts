import { Inject } from '@nestjs/common';
import { TOKEN_API_CONSUMER } from './apiConsumer.const';

export const tokensApisConsumers: string[] = new Array<string>();
/**
 *
 * @param tokenApi Identify TOKEN the implementation
 * @returns Return undefined if tokenApi not have a implementation otherwise
 * return the implementation
 *
 */
export function ApiConsumer(tokenApi: string) {
  if (!TOKEN_API_CONSUMER.hasOwnProperty(tokenApi)) {
    //console.log(`Not exist provider for ${tokenApi}`);
    throw new Error(`Not exist provider for ${tokenApi}`);
  }
  if (!tokensApisConsumers.includes(tokenApi)) {
    tokensApisConsumers.push(tokenApi);
  }
  return Inject(TOKEN_API_CONSUMER[tokenApi].className);
}
