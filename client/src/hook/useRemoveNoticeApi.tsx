import { useContext, useEffect } from "react";
import NoticeContext, {
  AppContextInterfaceNotice,
} from "../store/NoticeContext";

const serverApi = process.env.REACT_APP_API + "/api/removenotice/";
/**
 * @description Is a hook that listener actionDeleteNotice
 * @returns A string that is the actions is run, only for debug
 */
function useRemoveNoticeApi(): string {
  const { actionDeleteNotice, setActDeleteNotice } =
    useContext<AppContextInterfaceNotice>(NoticeContext);
  //console.log("Call to useRemoveNoticeApi");
  const actionDeleteNot = actionDeleteNotice();
  useEffect(() => {
    if (actionDeleteNot) {
      //console.log(`Send remove notice to api notice: ${actionDeleteNotice()}`);
      fetch(serverApi + `${actionDeleteNot}`, {})
        .then((res) => {
          // console.log("return call fetch");
          return res.json();
        })
        .then((data) => {
          // console.log(`${data}`);
        })
        .catch((error) => {
          //  console.log(`Error send remove notice  from ${serverApi}: `, error);
        });
      setActDeleteNotice("");
    }
  }, [actionDeleteNotice]);
  return actionDeleteNot;
}

export default useRemoveNoticeApi;
