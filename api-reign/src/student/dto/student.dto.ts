import { ApiProperty } from '@nestjs/swagger';
export class CreateStudentDto {
  @ApiProperty()
  name: string;
  @ApiProperty()
  teacher: string;
}

export class UpdateStudentDto {
  @ApiProperty()
  name: string;
  @ApiProperty()
  teacher: string;
}

export class FindStudentResponceDto {
  _id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  teacher: string;
}

export class StudentResponceDto {
  _id: string;
  @ApiProperty()
  name: string;
  @ApiProperty()
  teacher: string;
}
