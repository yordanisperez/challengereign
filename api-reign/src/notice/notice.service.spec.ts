import { Test, TestingModule } from '@nestjs/testing';

import { getConnectionToken, getModelToken } from '@nestjs/mongoose';

import { NoticeDto } from './dto/notice.dto';
import { ApiReignNotice } from './notice.service';

import { consumerApiAxios } from './http/apiConsumerAxios';

describe('StudentService', () => {
  const noticeDto: NoticeDto[] = [
    {
      _id: '10',
      created_at: new Date('2021-07-07T16:00:20.000Z'),
      title: 'This is Title 1',
      url: null,
      author: 'This is a author 1',
      story_title: 'This a story title 1',
      story_url: 'This a story url 1',
    },
  ];

  jest
    .spyOn(global, 'setTimeout')
    .mockImplementation(
      (accionFun: (arg: void) => void, time: number): NodeJS.Timeout => {
        return undefined;
      },
    );
  jest.spyOn(global, 'clearTimeout');

  const MockDocument = {
    save: jest.fn().mockImplementation(() => {
      return Promise.resolve({
        _id: 'Testing_ID',
        ...noticeDto[0],
      });
    }),
  };

  const MockModelNotice = {
    find: jest.fn().mockImplementation(() => {
      if (noticeDto.length === 0) return undefined;
      //const { name, teacher } = studentDto[0];
      return Promise.resolve(noticeDto);
    }),
    create: () => MockDocument,
    deleteOne: jest.fn().mockImplementation(() => {
      return Promise.resolve({
        deleteCount: 1,
      });
    }),
    deleteMany: jest.fn(),
  };

  const MockConsumerAxios = {
    getJsonApi: jest.fn().mockImplementation(() => {
      if (noticeDto.length === 0) return undefined;
      //const { name, teacher } = studentDto[0];
      return Promise.resolve(noticeDto);
    }),
  };

  const MockSeccion = {
    startTransaction: jest.fn(),
    abortTransaction: jest.fn(),
    endSession: jest.fn(),
  };

  const MockConection = {
    startSession: () => MockSeccion,
  };
  //student service
  let noticeService: ApiReignNotice;
  beforeEach(async () => {
    jest.clearAllMocks();
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ApiReignNotice,
        {
          provide: getModelToken('Notice'),
          useValue: MockModelNotice,
        },
        {
          provide: consumerApiAxios,
          useValue: MockConsumerAxios,
        },
        {
          provide: getConnectionToken('Database'),
          useValue: MockConection,
        },
      ],
    }).compile();

    // instantiate Simulated student service
    noticeService = module.get<ApiReignNotice>(ApiReignNotice);
  });

  it('01-Should be defined', () => {
    expect(noticeService).toBeDefined();
  });

  it('02-Should have getNotices function', () => {
    expect(noticeService.getNotices).toBeDefined();
  });

  it('03-The getNotices function should retrieve Promise<NoticeDto[]> ', async () => {
    expect(await noticeService.getNotices()).toMatchObject(noticeDto);
  });
  it('04-Should have deleteNotice function', () => {
    expect(noticeService.deleteNotice).toBeDefined();
  });
  it('05-The deleteNotice function should retrieve  Promise<DeleteNoticeDto> ', async () => {
    expect(await noticeService.deleteNotice('idNotice')).toMatchObject({
      deleteCount: 1,
    });
  });
  it('06-Should have inicialiceApi method', () => {
    expect(noticeService.inicialiceApi).toBeDefined();
  });
  it('07-The inicialiceApi method should call getJsonApi consumer API_CONSUMER_AXIOS and save data in Model Notice', async () => {
    await noticeService.inicialiceApi();
    expect(MockConsumerAxios.getJsonApi).toHaveBeenCalledTimes(1);
    expect(MockConsumerAxios.getJsonApi).toHaveBeenCalledWith(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
    expect(MockDocument.save).toHaveBeenCalledTimes(1);
  });
  it('08-Should have clearApi method', () => {
    expect(noticeService.clearApi).toBeDefined();
  });
  it('09-The clearApi method should call deleteMany of Model Notice', async () => {
    await noticeService.clearApi();
    expect(MockModelNotice.deleteMany).toHaveBeenCalledTimes(1);
  });
  it('10-Should have updateApi method', () => {
    expect(noticeService.updateApi).toBeDefined();
  });
  it('11-The updateApi method should call clearTimeout and setTimeout for new update', async () => {
    await noticeService.updateApi();
    expect(setTimeout).toBeCalledTimes(2); //1 in constructor and 1 in updateApi
    expect(clearTimeout).toBeCalledTimes(1);
    expect(MockSeccion.abortTransaction).toHaveBeenCalledTimes(0);
    expect(MockSeccion.startTransaction).toHaveBeenCalledTimes(1);
    expect(MockSeccion.endSession).toHaveBeenCalledTimes(1);
  });
});
