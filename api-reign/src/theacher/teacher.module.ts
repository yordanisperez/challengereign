import { Module } from '@nestjs/common';
import { StudentService } from '../student/student.service';
import { TeacherStudentController } from './student.controller';
import { TeacherService } from './teacher.service';
import { TeacherController } from './theacher.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { StudentSchema } from '../student/student.schema';
@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Student', schema: StudentSchema }]),
  ],
  controllers: [TeacherController, TeacherStudentController],
  providers: [StudentService, TeacherService],
})
export class TeacherModule {}
