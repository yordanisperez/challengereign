import { Schema, Document } from 'mongoose';

export const StudentSchema = new Schema({
  name: String,
  teacher: String,
});

export interface iStudent extends Document {
  _id: string;
  name: string;
  teacher: string;
}
