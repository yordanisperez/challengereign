import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import Footer from "./Footer";

describe("Testing Footer", () => {
  test("Footer is rendering", () => {
    const component = render(<Footer></Footer>);
    component.getByText("GitHub");
    component.getByText("Llamame");
    component.getByText("Mail");
    component.getByText("Copyright 2021, Original Challenge Reign");
  });
});
