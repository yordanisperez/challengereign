import "@testing-library/jest-dom/extend-expect";
import { render, RenderResult } from "@testing-library/react";

import { NoticeContextProvider } from "./store/NoticeContext";

import { fireEvent, waitFor } from "@testing-library/dom";
import { App } from "./App";
import { iNotice } from "./interface/notice.interface";

function setupFetchStub(data: iNotice[]) {
  return function fetchStub(_url: string) {
    return new Promise((resolve) => {
      resolve({
        json: () => Promise.resolve(data),
      });
    });
  };
}

describe("App", () => {
  const noticeData: iNotice[] = [
    {
      _id: "10",
      created_at: new Date("2021-07-07T16:00:20.000Z"),
      title: "This is Title",
      url: null,
      author: "This is a author",
      story_title: "This a story title 1",
      story_url: "This a story url 1",
    },
  ];
  let component: RenderResult;
  beforeEach(async () => {
    global.fetch = jest.fn().mockImplementation(setupFetchStub(noticeData));
    component = await waitFor(() =>
      render(
        <NoticeContextProvider>
          <App />
        </NoticeContextProvider>
      )
    );
  });

  test(" is rendering all app", async () => {
    expect(await component.findByText("This is Title")).toBeInTheDocument();
    expect(
      await component.findByText("-This is a author-")
    ).toBeInTheDocument();
    expect(await component.findByText("Wednesday, Jul 07")).toBeInTheDocument();
    expect(
      await component.findByText("Challenge Reign Full Stack Developed")
    ).toBeInTheDocument();
    expect(await component.findByText("Load now")).toBeInTheDocument();
    expect(await component.findByText("Facebook")).toBeInTheDocument();
    expect(await component.findByText("GitHub")).toBeInTheDocument();
    expect(
      await component.findByText("Copyright 2021, Original Challenge Reign")
    ).toBeInTheDocument();
  });

  test("When cliking in button remove notice ", async () => {
    const componentCliking = await component.findByText("This is Title");
    fireEvent.mouseOver(componentCliking);
    fireEvent.click(
      await component.findByRole("button", { name: "Delete Notice" })
    );
    await waitFor(() => {
      expect(component.queryByText("This is Title")).not.toBeInTheDocument();
    });
    await waitFor(() => {
      expect(
        component.queryByText("-This is a author-")
      ).not.toBeInTheDocument();
    });
  });

  test("When cliking in button 'Load Now' loading the notice ", async () => {
    const componentCliking = await component.findByText("This is Title");
    fireEvent.mouseOver(componentCliking);
    fireEvent.click(
      await component.findByRole("button", { name: "Delete Notice" })
    );
    await waitFor(() => {
      expect(component.queryByText("This is Title")).not.toBeInTheDocument();
    });
    await waitFor(() => {
      expect(
        component.queryByText("-This is a author-")
      ).not.toBeInTheDocument();
    });

    const loadNow = await component.findByText("Load now");
    fireEvent.click(loadNow);
    expect(await component.findByText("This is Title")).toBeInTheDocument();
    expect(
      await component.findByText("-This is a author-")
    ).toBeInTheDocument();
  });
});
