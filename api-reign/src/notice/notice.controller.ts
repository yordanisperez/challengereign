import { Get, Controller, Param } from '@nestjs/common';
import {
  DeleteNoticeDto,
  NoticeDto,
  ResponceDeleteNoticeDto,
} from './dto/notice.dto';

import { ApiReignNotice } from './notice.service';

@Controller('api')
export class ApiReignController {
  constructor(private readonly noticeService: ApiReignNotice) {}
  @Get('/notice')
  async getNotices(): Promise<NoticeDto[]> {
    return await this.noticeService.getNotices();
  }
  @Get('removenotice/:noticeId')
  async deleteNotice(
    @Param('noticeId') noticeId: string,
  ): Promise<ResponceDeleteNoticeDto> {
    let deleteCount: DeleteNoticeDto;
    try {
      deleteCount = await this.noticeService.deleteNotice(noticeId);
    } catch (error) {
      console.log(`The notice with noticeId = ${noticeId} not was deleted `);
    }

    if (deleteCount)
      return {
        state: 'success',
        message: `The notice with noticeId = ${noticeId} was deleted success`,
      };
    return {
      state: 'fail',
      message: `The notice with noticeId = ${noticeId} not was deleted, not exist`,
    };
  }
}
