import { Link } from "react-router-dom";
import { useContext } from "react";
import NoticeContext, {
  AppContextInterfaceNotice,
} from "../store/NoticeContext";

import logo from "../LogoReign.svg";
import "./navigation.scss";

function Navigation(): JSX.Element {
  const { setActionLoadData } =
    useContext<AppContextInterfaceNotice>(NoticeContext);

  return (
    <div id="header">
      <Link to="/">
        <img
          className="navigation-logo"
          src={logo}
          width="75"
          alt="Reign Logo"
        ></img>
      </Link>
      <h1>Challenge Reign Full Stack Developed</h1>
      <div></div>
      <nav className="navigation-nav">
        <ul className="navigation-ul">
          <li className="navigation-li">
            <Link
              className="navigation-link"
              to="/"
              onClick={() => setActionLoadData(true)}
            >
              Load now
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}

export default Navigation;
