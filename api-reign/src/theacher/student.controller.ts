import { Controller, Get, Param, ParseUUIDPipe, Put } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import {
  FindStudentResponceDto,
  StudentResponceDto,
} from 'src/student/dto/student.dto';
import { StudentService } from '../student/student.service';
@ApiTags('Teacher')
@Controller('teacher/:teacherId')
export class TeacherStudentController {
  constructor(private readonly studentService: StudentService) {}
  /**
   * @description This route retrieve all student that contain theacherId
   * @param {string} teacherId -id of teacher
   * @returns {FindStudentResponceDto[]} Array all student
   */

  @Get('/students')
  @ApiOperation({ summary: 'summary goes here' })
  @ApiResponse({
    status: 200,
    description:
      'description goes here' /*, schema: { ...define schema here... }*/,
  })
  async getTeacher(
    @Param('teacherId', new ParseUUIDPipe()) teacherId: string,
  ): Promise<FindStudentResponceDto[]> {
    return await this.studentService.getStudentForTeacherId(teacherId);
  }
  /**
   * @description -In this route is update the student identify studentId with  teacherID
   * @param {string} teacherId -id of teacher
   * @param {string} studentId -id of student
   * @returns {StudentResponceDto} Object student update responce
   */
  @Put('students/:studentId')
  async updateStudentTeacherId(
    @Param('teacherId') teacherId: string,
    @Param('studentId') studentId: string,
  ): Promise<StudentResponceDto> {
    return await this.studentService.updateStudentTeacherId(
      teacherId,
      studentId,
    );
  }
}
