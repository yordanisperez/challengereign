import { Document } from 'mongoose';
export interface iNotice extends Document {
  _id: string;
  created_at: Date;
  title: string;
  url: string;
  author: string;
  story_title: string;
  story_url: string;
}
