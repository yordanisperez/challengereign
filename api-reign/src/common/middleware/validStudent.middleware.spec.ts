import { INestApplication /*, ParseUUIDPipe*/ } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { FilterQuery } from 'mongoose';
import {
  CreateStudentDto,
  UpdateStudentDto,
} from '../../student/dto/student.dto';
import { iStudent } from '../../student/student.schema';
import * as request from 'supertest';

import { StudentModule } from '../../student/student.module';

describe('Middleware', () => {
  let app: INestApplication;
  const studentDto: CreateStudentDto = {
    name: 'yordanis',
    teacher: 'Id_Teacher',
  };
  const MockModelStudent = {
    findOne: jest.fn().mockImplementation((obj: FilterQuery<iStudent>) => {
      const { _id } = obj;
      //return _id;
      if (_id !== 'Testing_ID') return undefined;
      return Promise.resolve({
        _id,
        ...studentDto,
      });
    }),
    findByIdAndUpdate: jest
      .fn()
      .mockImplementation((_id: string, updateStd: UpdateStudentDto) => {
        if (_id !== 'Testing_ID') return undefined;
        return Promise.resolve({
          _id,
          ...updateStd,
        });
      }),
  };
  beforeEach(async () => {
    app = (
      await Test.createTestingModule({
        imports: [StudentModule],
      })
        .overrideProvider(getModelToken('Student'))
        .useValue(MockModelStudent)
        .compile()
    ).createNestApplication();

    await app.init();
  });

  it(`01-forRoutes get(students/:studentId), if is id valid return 200 with string test`, async () => {
    return await request(app.getHttpServer())
      .get('/students/Testing_ID')
      .expect(200, {
        _id: 'Testing_ID',
        ...studentDto,
      });
  });

  it(`02-forRoutes put(students/:studentId), if is id valid return 200 with string test`, async () => {
    return await request(app.getHttpServer())
      .put('/students/Testing_ID')
      .send(studentDto)
      .expect(200, {
        _id: 'Testing_ID',
        ...studentDto,
      });
  });

  it(`03-forRoutes get(students/:studentId) , if is not id valid return 400 with string student not found`, () => {
    return request(app.getHttpServer())
      .get('/students/Testing_NotValidID')
      .expect(400, { statusCode: 400, message: 'Student not found' });
  });

  it(`04-forRoutes put(students/:studentId), if is not id valid return 400 with string student not found`, () => {
    return request(app.getHttpServer())
      .put('/students/Testing_NotValidID')
      .expect(400, { statusCode: 400, message: 'Student not found' });
  });

  afterEach(async () => {
    await app.close();
  });
});
