const axios = require("axios");

function loadNotice(urlApi, done) {
  axios
    .get(urlApi)
    .then((response) => {
      done(null, response.data);
    })
    .catch((error) => {
      done(error, null);
    });
}

exports.loadNotice = loadNotice;
