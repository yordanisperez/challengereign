import { Injectable } from '@nestjs/common';
import { teachers } from '../db';
import { FindTeacherResponseDto } from './dto/teacher.dto';

@Injectable()
export class TeacherService {
  private teacher = teachers;
  /**
   *
   * @returns {FindTeacherResponseDto[]} Retrieve a array all the teacher
   */
  getTeachers(): FindTeacherResponseDto[] {
    return this.teacher;
  }
  /**
   *
   * @param {string} teacherId -id the el teacher to retrieve
   * @returns {FindTeacherResponseDto} The respose data teacher
   */
  getTeacherByID(teacherId: string): FindTeacherResponseDto {
    return this.teacher.find((itTeacher) => itTeacher.id === teacherId);
  }
}
