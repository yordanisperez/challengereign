import { Provider } from '@nestjs/common';
import { iConsumerApi } from '../interface/apiConsumer.interface';
import { TOKEN_API_CONSUMER } from './apiConsumer.const';
import { tokensApisConsumers } from './apiConsumer.decorator';
/**
 *
 * @param tokenApi string that identidy the implementation of abstract class iConsumerApi
 * @returns A provide with the implementation solve dinamic
 */
function createConsumerProvider(tokenApi: string): Provider<iConsumerApi> {
  return {
    provide: tokenApi,
    useFactory: (consumer) => consumer,
    inject: [
      TOKEN_API_CONSUMER[tokenApi].className,
      ...TOKEN_API_CONSUMER[tokenApi].dependOn,
    ],
  };
}
/**
 *
 * @returns a array with all implementations of provide for decorador apiConsumer
 */
export function createConsumersProviders(): Array<Provider<iConsumerApi>> {
  return tokensApisConsumers.map((tokenApi) => {
    return createConsumerProvider(tokenApi);
  });
}
/**
 *
 * @returns Array providers implementations
 */
export function createProviders(): Array<any> {
  return tokensApisConsumers.map((tokenApi) => {
    return TOKEN_API_CONSUMER[tokenApi].className;
  });
}
