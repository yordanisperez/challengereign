/** @type {import('webpack').Configuration} */
const webpack = require("webpack");
const { merge } = require("webpack-merge");
const common = require("./webpack.common");

const prodConfig = {
  // devtool: 'source-map',//eval or eval-source-map or source-map recomendado para production
  mode: "production", // production or development
  optimization: {
    moduleIds: "deterministic",
    splitChunks: {
      chunks: "all",
    },
    runtimeChunk: "single",
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env.REACT_APP_API": JSON.stringify(process.env.REACT_APP_API),
    }),
  ],
};
module.exports = merge(common, prodConfig);
