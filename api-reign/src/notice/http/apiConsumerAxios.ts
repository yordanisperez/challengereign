import { NoticeDto } from '../dto/notice.dto';
import { HttpService as HttpServiceAxios } from '@nestjs/axios';
import { lastValueFrom } from 'rxjs';
import { Injectable } from '@nestjs/common';
import { iConsumerApi } from '../interface/apiConsumer.interface';

@Injectable()
/**
 * A implementación update request from axios
 */
export class consumerApiAxios implements iConsumerApi {
  constructor(private httpService: HttpServiceAxios) {}
  setHttpService(httpService: any) {
    this.httpService = httpService;
  }
  async getJsonApi(url: string): Promise<NoticeDto[]> {
    const responseAxios = await lastValueFrom(this.httpService.get(url));

    const resp: NoticeDto[] = responseAxios.data.hits.map((itNot: any) => {
      const {
        created_at,
        title,
        url,
        author,
        story_title,
        story_url,
        ...rest
      } = itNot;
      return { created_at, title, url, author, story_title, story_url };
    });
    return resp;
  }
}
