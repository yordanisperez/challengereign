import { Test, TestingModule } from '@nestjs/testing';
import { TeacherDto } from './dto/teacher.dto';
import { TeacherService } from './teacher.service';
import { TeacherController } from './theacher.controller';

describe('TeacherController', () => {
  let controllerTeacher: TeacherController;

  const teacherDto: TeacherDto = {
    name: 'Teacher',
  };

  //Configure monk service
  const monkTeacherService = {
    getTeachers: jest.fn().mockImplementation(() => {
      return [
        {
          id: 'Testing_ID',
          ...teacherDto,
        },
      ];
    }),
    getTeacherByID: jest.fn().mockImplementation((id) => {
      return {
        id,
        ...teacherDto,
      };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TeacherController],
      providers: [
        {
          provide: TeacherService,
          useValue: monkTeacherService,
        },
      ],
    }).compile();
    // instantiate  controller
    controllerTeacher = module.get<TeacherController>(TeacherController);
  });

  it('01-Should be defined', () => {
    expect(controllerTeacher).toBeDefined();
  });
  it('02-The function getTeachers Should be defined', () => {
    expect(controllerTeacher.getTeachers).toBeDefined();
  });
  it('03-The function getTeacherByID Should be defined', () => {
    expect(controllerTeacher.getTeacherByID).toBeDefined();
  });

  it('04-The function getTeachers Should retrieve FindTeacherResponseDto[]', () => {
    expect(controllerTeacher.getTeachers()).toEqual([
      {
        id: 'Testing_ID',
        ...teacherDto,
      },
    ]);
  });
  it('05-The function getTeacherByID Should retrieve FindTeacherResponseDto', () => {
    expect(controllerTeacher.getTeacherByID('Testing_ID')).toEqual({
      id: 'Testing_ID',
      ...teacherDto,
    });
  });
});
