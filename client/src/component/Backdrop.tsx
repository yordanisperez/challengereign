import "./backdrop.scss";

declare interface BackdropProps {
  onCancel?: () => void;
}

function Backdrop({ onCancel }: BackdropProps): JSX.Element {
  return <div className="backdrop" onClick={onCancel} />;
}

export default Backdrop;
