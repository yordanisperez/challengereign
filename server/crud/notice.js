const Notice = require("../model/notice");

const createNewNotice = async function (noticeJson, done) {
  let doc = new Notice(noticeJson);
  await doc
    .save()
    .then((saveDoc) => {
      done(null, doc);
    })
    .catch((error) => {
      done(error, doc);
    });
  return doc;
};

const removeAllNotice = async (done) => {
  try {
    const docs = await Notice.deleteMany().exec();
    done(null, docs);
  } catch (error) {
    done(error, null);
  }
};
const getAllNoticeSort = async (done) => {
  try {
    const docs = await Notice.find().sort({ created_at: -1 }).exec();
    done(null, docs);
    return docs;
  } catch (error) {
    done(error, null);
  }
};

const removeNotice = async (idNotice, done) => {
  try {
    const docs = await Notice.deleteOne({ _id: idNotice }).exec();
    done(null, docs);
  } catch (error) {
    done(error, null);
  }
};

const removeNoticeForAuthor = async (author, done) => {
  try {
    const docs = await Notice.deleteOne({ author: author }).exec();
    done(null, docs);
  } catch (error) {
    done(error, null);
  }
};

exports.createNewNotice = createNewNotice;
exports.removeAllNotice = removeAllNotice;
exports.getAllNoticeSort = getAllNoticeSort;
exports.removeNotice = removeNotice;
exports.removeNoticeForAuthor = removeNoticeForAuthor;
