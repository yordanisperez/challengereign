const { Schema, model } = require("mongoose");

const NoticeSchema = new Schema({
  created_at: {
    type: Date,
    required: true,
  },
  title: {
    type: String,
    required: false,
  },
  url: {
    type: String,
    required: false,
  },
  author: {
    type: String,
    required: true,
  },
  points: {
    type: Number,
    required: false,
  },
  story_text: {
    type: String,
    required: false,
  },
  comment_text: {
    type: String,
    required: false,
  },
  num_comments: {
    type: Number,
    required: false,
  },
  story_id: {
    type: Number,
    required: false,
  },
  story_title: {
    type: String,
    required: false,
  },
  story_url: {
    type: String,
    required: false,
  },
  parent_id: {
    type: Number,
    required: false,
  },
  created_at_i: {
    type: Number,
    required: false,
  },
  _tags: [String],
  objectID: {
    type: String,
    required: false,
  },
  _highlightResult: {
    title: {
      value: {
        type: String,
        required: false,
      },
      matchLevel: {
        type: String,
        required: false,
      },
      matchedWords: [String],
    },
    url: {
      value: {
        type: String,
        required: false,
      },
      matchLevel: {
        type: String,
        required: false,
      },
      fullyHighlighted: {
        type: Boolean,
        required: false,
      },
      matchedWords: [String],
    },
    author: {
      value: {
        type: String,
        required: false,
      },
      matchLevel: {
        type: String,
        required: false,
      },
      matchedWords: [String],
    },
    comment_text: {
      value: {
        type: String,
        required: false,
      },
      matchLevel: {
        type: String,
        required: false,
      },
      fullyHighlighted: {
        type: Boolean,
        required: false,
        matchedWords: [String],
      },
    },
    story_title: {
      value: {
        type: String,
        required: false,
      },
      matchLevel: {
        type: String,
        required: false,
      },
      matchedWords: [String],
    },
    story_url: {
      value: {
        type: String,
        required: false,
      },
      matchLevel: {
        type: String,
        required: false,
      },
      matchedWords: [String],
    },
  },
});

module.exports = model("Notice", NoticeSchema);
