/**
 * Esta es una clase abstracta, que es proveida para la
 * implementacion del patron fachada en el uso de distintas
 * implementaciones.
 */

export abstract class iConsumerApi {
  abstract getJsonApi(url: string);
  abstract setHttpService(httpService: any);
}
