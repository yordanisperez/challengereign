import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import Layout from "./Layout";

jest.mock("src/layout/Navigation.tsx", () => {
  return {
    __esModule: true,
    default: () => {
      return <div>Navigation is Mocking</div>;
    },
  };
});

describe("Testing Layout", () => {
  test("Layout is rendering", () => {
    const component = render(
      <Layout>
        <h1>I am children</h1>
      </Layout>
    );
    component.getByText("I am children");
    component.getByText("Navigation is Mocking");
  });
});
