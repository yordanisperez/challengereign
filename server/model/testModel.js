const { Schema, model } = require("mongoose");

const testShema = new Schema({
  comment_text: {
    type: String,
    required: false,
  },
});

module.exports = model("Test", testShema);
