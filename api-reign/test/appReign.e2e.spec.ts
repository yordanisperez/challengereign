import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';

import { Model } from 'mongoose';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';

import { ConfigModule } from '@nestjs/config';
import { ApiReignModule } from '../src/notice/notice.module';
import { iNotice } from '../src/notice/interface/notice.interface';

describe('AppNotice (e2e)', () => {
  jest
    .spyOn(global, 'setTimeout')
    .mockImplementation(
      (accionFun: (arg: void) => void, time: number): NodeJS.Timeout => {
        return undefined;
      },
    );
  jest.spyOn(global, 'clearTimeout');

  let app: INestApplication;
  let mockNoticeModel: Model<iNotice>;
  const notices = [
    {
      _id: '615c5e2b256e34c8fc2eda1b',
      created_at: '2021-07-07T16:00:20.000Z',
      title: 'Node.js – v16.9.0',
      url: null,
      author: 'istingray',
      story_title:
        "ProtonMail deletes 'we don't log your IP' from website after activist arrested",
      story_url:
        'https://www.theregister.com/2021/09/07/protonmail_hands_user_ip_address_police/',
    },
    {
      _id: '615c5e2b256e34c8fc2eda1e',
      created_at: '2021-09-07T12:57:32.000Z',
      title: 'Node.js – v16.9.0',
      url: 'https://nodejs.org/en/blog/release/v16.9.0/',
      author: 'bricss',
      story_title: null,
      story_url: null,
    },
  ];
  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ApiReignModule,
        ConfigModule.forRoot({ envFilePath: `.${process.env.NODE_ENV}.env` }),
        MongooseModule.forRoot(
          `mongodb://${process.env.HOST}/${process.env.DATABASE}`,
          {
            useNewUrlParser: true,
            useUnifiedTopology: true,
          },
        ),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    mockNoticeModel = moduleFixture.get<Model<iNotice>>(
      getModelToken('Notice'),
    );

    for (let i = 0; i < notices.length; i++) {
      const newNotice: iNotice = new mockNoticeModel(notices[i]);
      await newNotice.save();
    }
  });

  afterEach(async () => {
    await mockNoticeModel.deleteMany();
  });

  it('01-/ (GET api/notice)', async () => {
    return await request(app.getHttpServer())
      .get('/api/notice')
      .expect(200)
      .expect((responce) => {
        expect(responce.body).toMatchObject(notices);
      });
  });

  it('02-/ (GET api/removenotice/:noticeId)', async () => {
    const noticeId = '615c5e2b256e34c8fc2eda1b';
    return await request(app.getHttpServer())
      .get(`/api/removenotice/${noticeId}`)
      .expect(200)
      .expect((responce) => {
        expect(responce.body).toMatchObject({
          state: 'success',
          message: `The notice with noticeId = ${noticeId} was deleted success`,
        });
      });
  });
});
