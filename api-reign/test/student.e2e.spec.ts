import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';

import { iStudent } from '../src/student/student.schema';
import { Model } from 'mongoose';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { StudentModule } from '../src/student/student.module';
import { TeacherModule } from '../src/theacher/teacher.module';
import { ConfigModule } from '@nestjs/config';
//import { getModelToken } from '@nestjs/mongoose';

describe('AppStudent (e2e)', () => {
  let app: INestApplication;
  let mockStudentModel: Model<iStudent>;
  const students = [
    {
      _id: '615c5e2b256e34c8fc2eda1b',
      name: 'Anthony Cole',
      teacher: '9c9324e8-b656-11eb-8529-0242ac130003',
    },
    {
      _id: '615c5e2b256e34c8fc2eda1e',
      name: 'Micheal Bryant',
      teacher: '9c9324e8-b656-11eb-8529-0242ac130003',
    },
  ];
  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        StudentModule,
        TeacherModule,
        ConfigModule.forRoot({ envFilePath: `.${process.env.NODE_ENV}.env` }),
        MongooseModule.forRoot(
          `mongodb://${process.env.HOST}/${process.env.DATABASE}`,
          {
            useNewUrlParser: true,
            useUnifiedTopology: true,
          },
        ),
      ],
    })
      //  .overrideProvider(getModelToken('Student'))
      //   .useValue(mockStudentModel)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    mockStudentModel = moduleFixture.get<Model<iStudent>>(
      getModelToken('Student'),
    );
    //@InjectModel('Student')
    // let studentModel: Model<iStudent>;

    for (let i = 0; i < students.length; i++) {
      const newStudent: iStudent = new mockStudentModel(students[i]);
      await newStudent.save();
    }
  });

  afterEach(async () => {
    await mockStudentModel.deleteMany();
  });

  it('01-/ (GET)', async () => {
    return await request(app.getHttpServer())
      .get('/students')
      .expect(200, students);
  });

  it('02-/ (GET/:studentID)', async () => {
    return await request(app.getHttpServer())
      .get('/students/615c5e2b256e34c8fc2eda1e')
      .expect(200, students[1]);
  });

  it('03-/students (POST)', async () => {
    return await request(app.getHttpServer())
      .post('/students')
      .send({
        name: 'Yordanis Pérez',
        teacher: '9c9324e8-b656-11eb-8529-0242ac130003',
      })
      .expect(201)
      .expect(function (res) {
        res.body.teacher = '9c9324e8-b656-11eb-8529-0242ac130003';
        res.body.name = 'Yordanis Pérez';
        res.body._id = expect.any(String);
      });
  });

  it('04-/students (PUT)', async () => {
    return await request(app.getHttpServer())
      .put('/students/615c5e2b256e34c8fc2eda1e')
      .send({
        name: 'Yordanis Pérez',
        teacher: '9c9324e8-b656-11eb-8529-0242ac130003',
      })
      .expect(200)
      .expect(function (res) {
        res.body.teacher = '9c9324e8-b656-11eb-8529-0242ac130003';
        res.body.name = 'Yordanis Pérez';
        res.body._id = '615c5e2b256e34c8fc2eda1e';
      });
  });
});
